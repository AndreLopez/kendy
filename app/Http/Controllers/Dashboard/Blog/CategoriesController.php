<?php

namespace App\Http\Controllers\Dashboard\Blog;

use App\Http\Controllers\Controller;

use App\Category;

use App\Post;

use Illuminate\Http\Request;

use Validator;

class CategoriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories=Category::orderBy('id','DESC')->get();
        return view('blog.categories')->with('categories',json_encode($categories));
    }
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'name'=>'required',
            'slug'=>'required|unique:categories,slug',
        ]);
        if ($validator->fails()) {
           return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
        }  
        $category=Category::create($request->all());
        $categories=Category::orderBy('id','DESC')->get();
        return response()->json(['status'=>'success','mensaje'=>'Registro Satisfactorio.', 'categories'=>$categories]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $validator=Validator::make($request->all(), [
            'name'=>'required',
            'slug'=>'required',
        ]);
        if ($validator->fails()) {
           return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
        }  
        $Category=Category::where('id','<>',$request->id)->where('slug',$request->slug)->get();
        $error=array('No se puede duplicar el campo slug.');
        if(count($Category)!=0){
            return response()->json(['status'=>'error','mensaje'=>$error]);  
        }// if(count($Service)!=0)
        $Category=Category::where('id',$request->id)->first();
        $Category->name=$request->name;
        $Category->slug=$request->slug;
        $Category->body=$request->body;
        $Category->update();
        $categories=Category::orderBy('id','DESC')->get();
        return response()->json(['status'=>'success','mensaje'=>'Actualización Satisfactoria.', 'categories'=>$categories]);
    }
    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Category=Category::where('id',$request->id)->first();
        $Post=Post::where('category_id',$request->id)->first();
        if(count($Post)>0){
            return response()->json(['status'=>'error','mensaje'=>['El registro que desea eliminar se encuentra asociado a una entrada']]);
        }//if(count($Post)>0)
        else if (count($Category)>0) {
            $Category->delete();
            $categories=Category::orderBy('id','DESC')->get();
            return response()->json(['status'=>'success','mensaje'=>'Registro eliminado satisfactoriamente.', 'categories'=>$categories]);
        }//else if (count($Category)>0)
         else {
          return response()->json(['status'=>'error','mensaje'=>'El registro que desea eliminar no se encuentra en base de datos']);
        }
    }
}

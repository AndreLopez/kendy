<?php

namespace App\Http\Controllers\Dashboard\Blog;

use Illuminate\Http\Request;
use App\Http\Requests\PostStoreRequest;
use App\Http\Requests\PostUpdateRequest;
use App\Http\Requests\PostDestroyRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Post;
use App\Category;
use App\Tag;
use Validator;


class PostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts=[];
        $posts=Post::join('users','users.id','posts.user_id')
                    ->where('users.id','=',auth()->id())->orderBy('posts.id','DESC')
                    ->select('posts.id','posts.name','posts.category_id','posts.slug','posts.excerpt','posts.body','posts.status','posts.image_name','posts.file','posts.created_at','users.id as user_id','users.name as user_name')->get();
        
        foreach($posts  as &$post){
            $post->tags()->pluck('name');
            $post['tags']=$post->tags()->pluck('tag_id');
        }//foreach($posts  as &$post)
        $tags=Tag::orderBy('name','ASC')->get();
        $categories=Category::orderBy('name','ASC')->get();     
 
        if(count($tags)==0)
          return redirect()->route('tags')->with('warning','Debe ingresar por lo menos una etiqueta');
        if(count($categories)==0)
          return redirect()->route('categories')->with('warning','Debe ingresar por lo menos una categoria');
        return view('blog.posts',compact('posts','categories','tags'));
    }
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostStoreRequest $request)
    {
        if ($request->file != "")
        {
            $formato = explode(";", $request->file);
            $formato = explode(";", $formato[0]);
            $formato = explode("/", $formato[0]);
            $formato = $formato[1];
            if ($formato != "png" && $formato != "svg+xml" && $formato != "jpeg")
            {
                return response()->json(['status' => 'error', 'mensaje' => ['El formato utilizado para el campo logo no esta permitida, solo admite formatos jpg y png.']]);
            } //if($formato!="png" && $formato!="svg+xml" && $formato!="jpeg")
        } //if($request->file!="")

        $post=Post::create($request->all());       
        if($request->file('file')){
           $post->fill($request->file)->save();
        }//if($request->file('file'))
     
        $post->tags()->attach($request->get('tags'));
        $posts=Post::join('users','users.id','posts.user_id')
                          ->where('users.id','=',auth()->id())->orderBy('posts.id','DESC')
                          ->select('posts.id','posts.name','posts.category_id','posts.slug','posts.excerpt','posts.body','posts.status','posts.image_name','posts.file','posts.created_at','users.id as user_id','users.name as user_name')->get();
        foreach($posts  as &$post){
         $post->tags()->pluck('name');
         $post['tags']=$post->tags()->pluck('tag_id');                      
        }//foreach($posts  as &$post)
        $tags=Tag::orderBy('name','ASC')->get();
        $categories=Category::orderBy('name','ASC')->get();
        return response()->json(['status'=>'success','mensaje'=>'Registro Satisfactorio.', 'posts'=>$posts,'categories'=>$categories,'tags'=>$tags]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $validator=Validator::make($request->all(), [
            'id'         =>'required |integer',
            'name'       =>'required',
            'slug'       =>'required |unique:posts,slug,'. $request->id,
            'user_id'    =>'required |integer',
            'category_id'=>'required |integer',
            'tags'       =>'required |array',
            'body'       =>'required',
            'status'     =>'required|in:DRAFT,PUBLISHED',
        ]);
        if ($validator->fails()) {
           return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
        }//if ($validator->fails())  

        if ($request->file != "")
        {
            $formato = explode(";", $request->file);
            $formato = explode(";", $formato[0]);
            $formato = explode("/", $formato[0]);
            $formato = $formato[1];
            if ($formato != "png" && $formato != "svg+xml" && $formato != "jpeg")
            {
                return response()->json(['status' => 'error', 'mensaje' => ['El formato utilizado para el campo logo no esta permitida, solo admite formatos jpg y png.']]);
            } //if($formato!="png" && $formato!="svg+xml" && $formato!="jpeg")
        } //if($request->file!="")

        
        $post=Post::find($request->id);
        
        $this->authorize('pass',$post);
        $post->fill($request->all())->save();

        if($request->file('file')){
            $post->fill($request->file)->save();
         }//if($request->file('file'))
      
         $post->tags()->sync($request->get('tags')); 
         $posts=Post::join('users','users.id','posts.user_id')
                    ->where('users.id','=',auth()->id())->orderBy('posts.id','DESC')
                   ->select('posts.id','posts.name','posts.category_id','posts.slug','posts.excerpt','posts.body','posts.status','posts.image_name','posts.file','posts.created_at','users.id as user_id','users.name as user_name')->get();
         foreach($posts  as &$post){
            $post->tags()->pluck('name');
            $post['tags']=$post->tags()->pluck('tag_id');                      
         }//foreach($posts  as &$post)
         $tags=Tag::orderBy('name','ASC')->get();
         $categories=Category::orderBy('name','ASC')->get();
         return response()->json(['status'=>'success','mensaje'=>'Actualización Satisfactoria.', 'posts'=>$posts,'categories'=>$categories,'tags'=>$tags]);
    }
    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PostDestroyRequest $request)
    {
        $post=Post::find($request->id);
        $this->authorize('pass',$post);
        $post->delete();
        $posts=Post::join('users','users.id','posts.user_id')
                ->orderBy('posts.id','DESC')
                ->select('posts.id','posts.name','posts.category_id','posts.slug','posts.excerpt','posts.body','posts.status','posts.file','posts.created_at','users.id as user_id','users.name as user_name')->get();
        foreach($posts  as &$post){
         $post->tags()->pluck('name');
         $post['tags']=$post->tags()->pluck('tag_id');                      
        }//foreach($posts  as &$post)
        $tags=Tag::orderBy('name','ASC')->get();
        $categories=Category::orderBy('name','ASC')->get();        
        return response()->json(['status'=>'success','mensaje'=>'Registro eliminado satisfactoriamente', 'posts'=>$posts,'categories'=>$categories,'tags'=>$tags]);
    }
}

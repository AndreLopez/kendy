<?php

namespace App\Http\Controllers\Dashboard\Blog;

use App\Http\Controllers\Controller;

use App\Tag;

use App\Post;

use Illuminate\Http\Request;

use Validator;

class TagsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags=Tag::orderBy('id','DESC')->get();
        return view('blog.tags')->with('tags',json_encode($tags));
    }
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'name'=>'required',
            'slug'=>'required|unique:tags,slug',
        ]);
        if ($validator->fails()) {
           return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
        }  
        $tag=Tag::create($request->all());
        $tags=Tag::orderBy('id','DESC')->get();
        return response()->json(['status'=>'success','mensaje'=>'Registro Satisfactorio.', 'tags'=>$tags]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'name'=>'required',
            'slug'=>'required',
        ]);
        if ($validator->fails()) {
           return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
        }  
        $Tag=Tag::where('id','<>',$request->id)->where('slug',$request->slug)->get();
        $error=array('No se puede duplicar el campo slug.');
        if(count($Tag)!=0){
            return response()->json(['status'=>'error','mensaje'=>$error]);  
        }// if(count($Service)!=0)
        $Tag=Tag::where('id',$request->id)->first();
        $Tag->name=$request->name;
        $Tag->slug=$request->slug;
        $Tag->update();
        $tags=Tag::orderBy('id','DESC')->get();
        return response()->json(['status'=>'success','mensaje'=>'Actualización Satisfactoria.', 'tags'=>$tags]);
    }
    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Tag=Tag::where('id',$request->id)->first();
        $posts=Post::join('users','users.id','posts.user_id')
                    ->where('users.id','=',auth()->id())->orderBy('posts.id','DESC')
                    ->select('posts.id','posts.name','posts.category_id','posts.slug','posts.excerpt','posts.body','posts.status','posts.image_name','posts.file','posts.created_at','users.id as user_id','users.name as user_name')->get();
        $find=0;
        foreach($posts  as &$post){
            $post->tags()->pluck('name');
            $post['tags']=$post->tags()->pluck('tag_id');
            if($post['tags'][0]==$request->id);
             $find=1;
        }//foreach($posts  as &$post)
        
        if($find==1)
          return response()->json(['status'=>'error','mensaje'=>['El registro que desea eliminar se encuentra asociado a una entrada']]);
        if (count($Tag)>0 ) {
            $Tag->delete();
            $tags=Tag::orderBy('id','DESC')->get();
            return response()->json(['status'=>'success','mensaje'=>'Registro eliminado satisfactoriamente.', 'tags'=>$tags]);
        } else {
          return response()->json(['status'=>'error','mensaje'=>'El registro que desea eliminar no se encuentra en base de datos']);
        }
    }
}

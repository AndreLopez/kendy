<?php

namespace App\Http\Controllers\Dashboard\Settings;

use App\Http\Controllers\Controller;

use App\User;

use App\Post;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;

use App\Http\Requests\UserStoreRequest;

use Validator;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=User::orderBy('id','DESC')->get();
        return view('settings.users')->with('users',json_encode($users));
    }
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'name'       =>'required',
            'email'       =>'required |unique:users,email', 
            'password'       =>'required |string | min:6',             
            'rol'     =>'required|in:Administrador,Usuario',
        ]);
        if ($validator->fails()) {
           return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
        }  
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'rol' => $request->rol,
        ]);
        $users=User::orderBy('id','DESC')->get();
        return response()->json(['status'=>'success','mensaje'=>'Registro Satisfactorio.', 'users'=>$users]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $arrayValidator=array(
            'name'       =>'required',
            'email'       =>'required ',              
            'rol'     =>'required|in:Administrador,Usuario');
        if($request->password!=""){
            $arrayValidator=array_merge($arrayValidator,['password'    =>'required |string | min:6']); 
        }//if($request->password!="")
        $validator=Validator::make($request->all(), $arrayValidator);
        if ($validator->fails()) {
           return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
        }// if ($validator->fails())   
        $User=User::where('id','<>',$request->id)->where('email',$request->email)->get();
        $error=array('No se puede duplicar el campo correo electrónico.');
        if(count($User)!=0){
            return response()->json(['status'=>'error','mensaje'=>$error]);  
        }// if(count($User)!=0)
        $User=User::where('id',$request->id)->first();
        $User->name=$request->name;
        $User->email=$request->email;
        if($request->password!="")
          $User->password=Hash::make($request->password);
        $User->rol=$request->rol;
        $User->update();
        $users=User::orderBy('id','DESC')->get();
        return response()->json(['status'=>'success','mensaje'=>'Actualización Satisfactoria.', 'users'=>$users]);
    }
    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $user=User::where('id',$request->id)->first();
        $Post=Post::where('user_id',$request->id)->first();
        if(count($Post)>0){
            return response()->json(['status'=>'error','mensaje'=>['El registro que desea eliminar se encuentra asociado a una entrada']]);
        }//if(count($Post)>0)
        else if (count($user)>0) {
            $user->delete();
            $users=User::orderBy('id','DESC')->get();
            return response()->json(['status'=>'success','mensaje'=>'Registro eliminado satisfactoriamente.', 'users'=>$users]);
        }//else if (count($User)>0)
         else {
          return response()->json(['status'=>'error','mensaje'=>'El registro que desea eliminar no se encuentra en base de datos']);
        }
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        return view('settings.profile');
    }
     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function loadUser()
    {
        $user=User::where('id',auth()->id())->first();        
        return response()->json(['user'=>$user]);
    }
}

<?php

namespace App\Http\Controllers\Dashboard\store;

use App\Http\Controllers\Controller;

use App\Coin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;

use App\Http\Requests\coinstoreRequest;

use Validator;

class CoinsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coins=Coin::orderBy('id','DESC')->get();
        foreach($coins  as &$coin){
           if($coin->position=="RIGHT")
            $coin['positionName']="Derecha";
           else
            $coin['positionName']="Izquierda";

           if($coin->status=="ACTIVE")
            $coin['statusName']="Activo";
           else
            $coin['statusName']="Inactivo";
        }//foreach($coins  as &$coin)
        return view('store.coins')->with('coins',json_encode($coins));
    }
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'name' => 'required|min:3|max:20',
            'abbreviation' =>'required|min:1|max:5',
            'symbol' =>'required',
            'position' =>'required',
            'status' =>'required|',
        ],[
           'name.required'=>'Existen campos por llenar:*Nombre.',
           'name.min'=>'El campo nombre acepta un minimo de 3 caracteres y un máximo de 20 caracteres.',
           'name.max'=>'El campo nombre acepta un minimo de 3 caracteres y un máximo de 20 caracteres.',
           'abbreviation.required'=>'Existen campos por llenar:*Abreviaciòn.',
           'abbreviation.min'=>'El campo Abreviaciòn acepta un minimo de 1 caracteres y un máximo de 5 caracteres.',
           'abbreviation.max'=>'El campo Abreviaciòn acepta un minimo de 1 caracteres y un máximo de 5 caracteres.',
           'symbol.required'=>'Existen campos por llenar:*Simbolo.',
           'position.required'=>'Existen campos por llenar:*Posición.',
           'status.required'=>'Existen campos por llenar:*Estado.',
        ]);
        if ($validator->fails()) {
           return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
        }  
        $Coin=Coin::create($request->all());
      
        if($request->status=='ACTIVE')
            Coin::where('id', '<>', $Coin->id)->update(array('status' => 'INACTIVE'));
          
        $coins=Coin::orderBy('id','DESC')->get();
        foreach($coins  as &$coin){
            if($coin->position=="RIGHT")
             $coin['positionName']="Derecha";
            else
             $coin['positionName']="Izquierda";
 
            if($coin->status=="ACTIVE")
             $coin['statusName']="Activo";
            else
             $coin['statusName']="Inactivo";
         }//foreach($coins  as &$coin)
        return response()->json(['status'=>'success','mensaje'=>'Registro Satisfactorio.', 'coins'=>$coins]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'name' => 'required|min:3|max:20',
            'abbreviation' =>'required|min:1|max:3',
            'symbol' =>'required',
            'position' =>'required',
            'status' =>'required|',
        ],[
           'name.required'=>'Existen campos por llenar:*Nombre.',
           'name.min'=>'El campo nombre acepta un minimo de 3 caracteres y un máximo de 20 caracteres.',
           'name.max'=>'El campo nombre acepta un minimo de 3 caracteres y un máximo de 20 caracteres.',
           'abbreviation.required'=>'Existen campos por llenar:*Abreviaciòn.',
           'abbreviation.min'=>'El campo Abreviaciòn acepta un minimo de 1 caracteres y un máximo de 5 caracteres.',
           'abbreviation.max'=>'El campo Abreviaciòn acepta un minimo de 1 caracteres y un máximo de 5 caracteres.',
           'symbol.required'=>'Existen campos por llenar:*Simbolo.',
           'position.required'=>'Existen campos por llenar:*Posición.',
           'status.required'=>'Existen campos por llenar:*Estado..',
        ]);
        if ($validator->fails()) {
           return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
        }  
        $Coin=Coin::where('id','<>',$request->id)->where('name',$request->name)->get();
        $error=array('No se puede duplicar el campo nombre.');
        if(count($Coin)!=0){
            return response()->json(['status'=>'error','mensaje'=>$error]);  
        }// if(count($Coin)!=0)
        $Coin=Coin::where('id',$request->id)->first();
        $Coin->fill($request->all())->save();
        if($request->status=='ACTIVE')
           Coin::where('id', '<>', $Coin->id)->update(array('status' => 'INACTIVE'));
        $coins=Coin::orderBy('id','DESC')->get();
        foreach($coins  as &$coin){
            if($coin->position=="RIGHT")
             $coin['positionName']="Derecha";
            else
             $coin['positionName']="Izquierda";
 
            if($coin->status=="ACTIVE")
             $coin['statusName']="Activo";
            else
             $coin['statusName']="Inactivo";
         }//foreach($coins  as &$coin)
        return response()->json(['status'=>'success','mensaje'=>'Actualización Satisfactoria.', 'coins'=>$coins]);
    }
    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Coin=Coin::where('id',$request->id)->first();
        if (count($Coin)>0) {
            $Coin->delete();
            $coins=Coin::orderBy('id','DESC')->get();
            foreach($coins  as &$coin){
                if($coin->position=="RIGHT")
                 $coin['positionName']="Derecha";
                else
                 $coin['positionName']="Izquierda";
     
                if($coin->status=="ACTIVE")
                 $coin['statusName']="Activo";
                else
                 $coin['statusName']="Inactivo";
             }//foreach($coins  as &$coin)
            return response()->json(['status'=>'success','mensaje'=>'Registro eliminado satisfactoriamente.', 'coins'=>$coins]);
        }//else if (count($Coin)>0)
         else {
          return response()->json(['status'=>'error','mensaje'=>'El registro que desea eliminar no se encuentra en base de datos']);
        }
    }
}

<?php

namespace App\Http\Controllers\Dashboard\store;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;

class OrdersController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $orders=Order::orderBy('id','desc')->get();
        if(count($orders)==0)
          return redirect()->route('dashboard')->with('warning','No hay ordenes que mostrar');
        return view('store.orders')->with('orders',json_encode($orders));
    }
     /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Order=Order::where('id',$request->id)->first();
        if (count($Order)>0) {
            $Order->delete();
            $Orders=Order::orderBy('id','DESC')->get();
            return response()->json(['status'=>'success','mensaje'=>'Registro eliminado satisfactoriamente.', 'Orders'=>$Orders]);
        }//else if (count($Order)>0)
         else {
          return response()->json(['status'=>'error','mensaje'=>'El registro que desea eliminar no se encuentra en base de datos']);
        }
    }
}

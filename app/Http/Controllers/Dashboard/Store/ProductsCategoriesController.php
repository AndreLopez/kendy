<?php

namespace App\Http\Controllers\Dashboard\Store;

use App\Http\Controllers\Controller;

use App\ProductCategory;

use App\Product;

use Illuminate\Http\Request;

use Validator;

class ProductsCategoriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productsCategories=[];
        $productsCategories=ProductCategory::orderBy('id','DESC')->get();
        foreach($productsCategories  as &$productCategory){
            $ProductCategory=ProductCategory::where('id',$productCategory->parent_id)->first();
            if(count($ProductCategory)> 0)
                $productCategory['parent_name']=$ProductCategory->name;
            else
                $productCategory['parent_name']="Sin categoría padre";
        }//foreach($productsCategories  as &$productCategory)
        $categoriesFatherProduct=ProductCategory::orderBy('id','DESC')->where('parent_id','=',0)->get();
        return view('store.productsCategories')->with(['productsCategories'=>json_encode($productsCategories),'categoriesFatherProduct'=>json_encode($categoriesFatherProduct)]);
    }
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'parent_id'=>'required|integer',
            'name'=>'required',
            'slug'=>'required|unique:product_categories,slug',
        ]);
        if ($validator->fails()) {
           return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
        }  
        $ProductCategory=ProductCategory::create($request->all());
        $productsCategories=ProductCategory::orderBy('id','DESC')->get();
        foreach($productsCategories  as &$productCategory){
            $ProductCategory=ProductCategory::where('parent_id',$productCategory->id)->first();
            if(count($ProductCategory)>0)
                $productCategory['parent_name']=$ProductCategory->name;
            else
                $productCategory['parent_name']="Sin categoría padre";
        }//foreach($productsCategories  as &$productCategory)
        $categoriesFatherProduct=ProductCategory::orderBy('id','DESC')->where('parent_id','=',0)->get();
        return response()->json(['status'=>'success','mensaje'=>'Registro Satisfactorio.', 'productsCategories'=>$productsCategories,'categoriesFatherProduct'=>$categoriesFatherProduct]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $validator=Validator::make($request->all(), [
            'parent_id'=>'required|integer',
            'name'=>'required',
            'slug'=>'required',
        ]);
        if ($validator->fails()) {
           return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
        }  
        $ProductCategory=ProductCategory::where('id','<>',$request->id)->where('slug',$request->slug)->get();
        $error=array('No se puede duplicar el campo slug.');
        if(count($ProductCategory)!=0){
            return response()->json(['status'=>'error','mensaje'=>$error]);  
        }// if(count($Service)!=0)
        $ProductCategory=ProductCategory::where('id',$request->id)->first();
        $ProductCategory->parent_id=$request->parent_id;
        $ProductCategory->name=$request->name;
        $ProductCategory->slug=$request->slug;
        $ProductCategory->body=$request->body;
        $ProductCategory->update();
        $productsCategories=ProductCategory::orderBy('id','DESC')->get();
        foreach($productsCategories  as &$productCategory){
            $ProductCategory=ProductCategory::where('parent_id',$productCategory->id)->first();
            if(count($ProductCategory)>0)
                $productCategory['parent_name']=$ProductCategory->name;
            else
                $productCategory['parent_name']="Sin categoría padre";
        }//foreach($productsCategories  as &$productCategory)
        $categoriesFatherProduct=ProductCategory::orderBy('id','DESC')->where('parent_id','=',0)->get();
        return response()->json(['status'=>'success','mensaje'=>'Actualización Satisfactoria.', 'productsCategories'=>$productsCategories,'categoriesFatherProduct'=>$categoriesFatherProduct]);
    }
    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $ProductCategory=ProductCategory::where('id',$request->id)->first();
        $Product=Product::where('product_category_id',$request->id)->first();
        if(count($Product)>0){
            return response()->json(['status'=>'error','mensaje'=>['El registro que desea eliminar se encuentra asociado a una entrada']]);
        }//if(count($Product)>0)
        else if (count($ProductCategory)>0) {
            $ProductCategory->delete();
            $productsCategories=ProductCategory::orderBy('id','DESC')->get();
            foreach($productsCategories  as &$productCategory){
                $ProductCategory=ProductCategory::where('parent_id',$productCategory->id)->first();
                if(count($ProductCategory)>0)
                    $productCategory['parent_name']=$ProductCategory->name;
                else
                    $productCategory['parent_name']="Sin categoría padre";
            }//foreach($productsCategories  as &$productCategory)
            $categoriesFatherProduct=ProductCategory::orderBy('id','DESC')->where('parent_id','=',0)->get();
            return response()->json(['status'=>'success','mensaje'=>'Registro eliminado satisfactoriamente.','productsCategories'=>$productsCategories,'categoriesFatherProduct'=>$categoriesFatherProduct]);
        }//else if (count($ProductCategory)>0)
         else {
          return response()->json(['status'=>'error','mensaje'=>'El registro que desea eliminar no se encuentra en base de datos']);
        }
    }
}

<?php

namespace App\Http\Controllers\Dashboard\store;

use App\Http\Controllers\Controller;

use App\Color;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;

use App\Http\Requests\colorstoreRequest;

use Validator;

class ProductsColorsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $colors=Color::orderBy('id','DESC')->get();
        return view('store.colors')->with('colors',json_encode($colors));
    }
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'name'       =>'required',
        ]);
        if ($validator->fails()) {
           return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
        }  
        Color::create($request->all());
        $colors=Color::orderBy('id','DESC')->get();
        return response()->json(['status'=>'success','mensaje'=>'Registro Satisfactorio.', 'colors'=>$colors]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'name'       =>'required',
        ]);
        if ($validator->fails()) {
           return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
        }  
        $Color=Color::where('id','<>',$request->id)->where('name',$request->name)->get();
        $error=array('No se puede duplicar el campo nombre.');
        if(count($Color)!=0){
            return response()->json(['status'=>'error','mensaje'=>$error]);  
        }// if(count($Color)!=0)
        $Color=Color::where('id',$request->id)->first();
        $Color->name=$request->name;
        $Color->update();
        $colors=Color::orderBy('id','DESC')->get();
        return response()->json(['status'=>'success','mensaje'=>'Actualización Satisfactoria.', 'colors'=>$colors]);
    }
    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Color=Color::where('id',$request->id)->first();
        if (count($Color)>0) {
            $Color->delete();
            $colors=Color::orderBy('id','DESC')->get();
            return response()->json(['status'=>'success','mensaje'=>'Registro eliminado satisfactoriamente.', 'colors'=>$colors]);
        }//else if (count($Color)>0)
         else {
          return response()->json(['status'=>'error','mensaje'=>'El registro que desea eliminar no se encuentra en base de datos']);
        }
    }
}

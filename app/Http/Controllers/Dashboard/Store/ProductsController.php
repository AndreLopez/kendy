<?php

namespace App\Http\Controllers\Dashboard\Store;

use App\Http\Controllers\Controller;

use App\Coin;

use App\ProductCategory;

use App\Color;

use App\Product;

use Illuminate\Http\Request;

use Validator;

class ProductsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Coin=Coin::where('status','=','ACTIVE')->first();
        $products=Product::orderBy('id','DESC')->get();
        foreach($products  as &$product){
            $ProductCategory=ProductCategory::where('id',$product->product_category_id)->first();
            $ProductSubCategory=ProductCategory::where('id',$product->product_category_parent_id)->first();
            if(count($ProductCategory)>0)
                $product['categoryName']=$ProductCategory->name;
            else
                $product['categoryName']="Sin categoría";
            if(count($ProductSubCategory)>0)
                $product['subCategoryName']=$ProductCategory->name;
            else
                $product['subCategoryName']="Sin subcategoría";
        }//foreach($products as &$product)
        $coins=Coin::orderBy('id','DESC')->where('status','=','ACTIVE')->get();
        if(count($coins)==0)
          return redirect()->route('coins')->with('warning','Debe ingresar por lo menos una moneda a utilizar para sus pagos ó en su caso activar la moneda a utilizar');
        $categoriesFatherProduct=ProductCategory::orderBy('id','DESC')->where('parent_id','=',0)->get();
        if(count($categoriesFatherProduct)==0)
          return redirect()->route('productsCategories')->with('warning','Debe ingresar por lo menos una categoría');
        $colors=Color::orderBy('id','DESC')->get();
        if(count($colors)==0)
          return redirect()->route('productsColors')->with('warning','Debe ingresar por lo menos un color para sus productos');
        return view('store.products')->with(['products'=>json_encode($products),'categoriesFatherProduct'=>json_encode($categoriesFatherProduct),'colors'=>json_encode($colors),'coin'=>json_encode($Coin)]);
    }
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'sku'=>'required|unique:products,sku,'. $request->sku,
            'product_category_id'=>'required|integer',
            'product_category_parent_id'=>'required|integer',
            'name'=>'required',
            'slug'=>'required|unique:products,slug,'. $request->slug,
            'name'=>'required',
            'color_id'=>'required',
            'description'=>'required',
            'price'=>'required',
            'visible'=>'required',
        ]);
      
        if ($validator->fails()) {
           return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
        }  

        if ($request->file != "")
        {
            $formato = explode(";", $request->file);
            $formato = explode(";", $formato[0]);
            $formato = explode("/", $formato[0]);
            $formato = $formato[1];
            if ($formato != "png" && $formato != "svg+xml" && $formato != "jpeg")
            {
                return response()->json(['status' => 'error', 'mensaje' => ['El formato utilizado para el campo logo no esta permitida, solo admite formatos jpg y png.']]);
            } //if($formato!="png" && $formato!="svg+xml" && $formato!="jpeg")
        } //if($request->file!="")
        $Product=Product::create($request->all());
        if($request->file('file')){
            $Product->fill($request->file)->save();
         }//if($request->file('file'))
        $products=Product::orderBy('id','DESC')->get();
        foreach($products  as &$product){
            $ProductCategory=ProductCategory::where('id',$product->product_category_id)->first();
            $ProductSubCategory=ProductCategory::where('id',$product->product_category_parent_id)->first();
            if(count($ProductCategory)>0)
                $product['categoryName']=$ProductCategory->name;
            else
                $product['categoryName']="Sin categoría";
            if(count($ProductSubCategory)>0)
                $product['subCategoryName']=$ProductCategory->name;
            else
                $product['subCategoryName']="Sin subcategoría";
        }//foreach($products as &$product)
        return response()->json(['status'=>'success','mensaje'=>'Registro Satisfactorio.', 'products'=>$products]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $validator=Validator::make($request->all(), [
            'sku'=>'required',
            'product_category_id'=>'required|integer',
            'product_category_parent_id'=>'required|integer',
            'name'=>'required',
            'slug'=>'required',
            'name'=>'required',
            'color_id'=>'required',
            'description'=>'required',
            'price'=>'required',
            'visible'=>'required',
        ]);

        if ($validator->fails()) {
           return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
        }  
        $Product=Product::where('id','<>',$request->id)->where('sku',$request->sku)->get();
        $error=array('No se puede duplicar el campo código del producto.');
        if(count($Product)!=0){
            return response()->json(['status'=>'error','mensaje'=>$error]);  
        }// if(count($Product)!=0)

        $Product=Product::where('id','<>',$request->id)->where('slug',$request->slug)->get();
        $error=array('No se puede duplicar el campo url amigable del producto.');
        if(count($Product)!=0){
            return response()->json(['status'=>'error','mensaje'=>$error]);  
        }// if(count($Product)!=0)
        $Product=Product::find($request->id);
        $Product->fill($request->all())->save();

        if($request->file('file')){
            $Product->fill($request->file)->save();
         }//if($request->file('file'))
        $products=Product::orderBy('id','DESC')->get();
        foreach($products  as &$product){
            $ProductCategory=ProductCategory::where('id',$product->product_category_id)->first();
            $ProductSubCategory=ProductCategory::where('id',$product->product_category_parent_id)->first();
            if(count($ProductCategory)>0)
                $product['categoryName']=$ProductCategory->name;
            else
                $product['categoryName']="Sin categoría";
            if(count($ProductSubCategory)>0)
                $product['subCategoryName']=$ProductCategory->name;
            else
                $product['subCategoryName']="Sin subcategoría";
        }//foreach($products as &$product)
        return response()->json(['status'=>'success','mensaje'=>'Actualización Satisfactoria.', 'products'=>$products]);
    }
    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Product=Product::where('id',$request->id)->first();
        if (count($Product)>0) {
            $Product->delete();
            $products=Product::orderBy('id','DESC')->get();
            foreach($products  as &$product){
                $ProductCategory=ProductCategory::where('id',$product->product_category_id)->first();
                $ProductSubCategory=ProductCategory::where('id',$product->product_category_parent_id)->first();
                if(count($ProductCategory)>0)
                    $product['categoryName']=$ProductCategory->name;
                else
                    $product['categoryName']="Sin categoría";
                if(count($ProductSubCategory)>0)
                    $product['subCategoryName']=$ProductCategory->name;
                else
                    $product['subCategoryName']="Sin subcategoría";
            }//foreach($products as &$product)
            return response()->json(['status'=>'success','mensaje'=>'Registro eliminado satisfactoriamente.','products'=>$products]);
        }//else if (count($ProductCategory)>0)
         else {
          return response()->json(['status'=>'error','mensaje'=>'El registro que desea eliminar no se encuentra en base de datos']);
        }
    }
    /**
     * Find the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function findSubCategories(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'id'=>'required|integer',
        ]);
        if ($validator->fails()) {
           return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
        }  
        $subCategories=ProductCategory::where('parent_id','=',$request->id)->orderBy('name','DESC')->get();
        return response()->json(['status'=>'success','mensaje'=>'Registros encontrados.','subCategories'=>$subCategories]);
    }

}

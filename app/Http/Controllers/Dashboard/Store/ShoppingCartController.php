<?php

namespace App\Http\Controllers\Dashboard\Store;

use App\Http\Controllers\Controller;

use Session; 

use App\Coin;

use App\ProductCategory;

use App\Color;

use App\Product;

use Illuminate\Http\Request;

use Validator;

class ShoppingCartController extends Controller
{
   
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function addShoppingCart(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'product_id' => 'required|integer',
        ],[
           'product_id.required'=>'El campo id del producto es requerido',
           'product_id.integer'=>'El id introducido es invalido',
        ]);
        if ($validator->fails()) {
           return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
        } 
        $product=Product::where('id',$request->product_id)->first();
        if(count($product)==0)
            return response()->json(['status'=>'error','mensaje'=>'El producto seleccionado no se encuentra registrado en la base de datos.']);
        try {   
            $this->insertShoppingCart($request->product_id);
        } catch (\Exception $e) { 
            return response()->json(['status'=>'error','Ocurrió un error: '.$e->getMessage()]);  
        }//catch()  
        
    }

    public function insertShoppingCart($product_id)
    {
        try {  
            $ShoppingCart = array();
            if(Session::get('ShoppingCart')==null){    
                $ShoppingCart[] = [
                    "id" => $product_id,
                    "product"=>$this->searchProduct($product_id),
                    "quantity" => 1,
                ];
                Session::put('ShoppingCart',$ShoppingCart);
            }else{
                $this->searchShoppingCart($product_id);   
            }
            return response()->json(['status'=>'success','mensaje'=>'Producto agregado.']);
        } catch (\Exception $e) { 
            return response()->json(['status'=>'error','Ocurrió un error: '.$e->getMessage()]);     
        }//catch()  
    }  
    
    public function searchShoppingCart($product_id)
    {
        try {  
            $ShoppingCart = array();
            $ShoppingCart =Session::get('ShoppingCart');
            $bandera=0;
            foreach ($ShoppingCart as &$product) {
                if($product['id']==$product_id){
                    $product['quantity']+=1;
                    $bandera=1;
                }
            }

            if($bandera==0)
                $ShoppingCart[] = [
                    "id" => $product_id,
                    "product"=>$this->searchProduct($product_id),
                    "quantity" => 1,
                ];
             Session::put('ShoppingCart',$ShoppingCart);
        } catch (\Exception $e) { 
            return response()->json(['status'=>'error','Ocurrió un error: '.$e->getMessage()]);  
        }//catch()       
    }

    public function searchProduct($product_id)
    {
        $product=Product::where('id',$product_id)->select('product_category_id','product_category_parent_id','color_id','name','slug','sku','description','price','file','image_name')->first();
        $product['colorName']=$product->Color->name;
        $product['categoryName']=$product->productCategory->name;
        if($product->productSubCategory->name!="")
            $product['subCategoryName']=$product->productSubCategory->name;
        else
            $product['subCategoryName']="Sin subcategoría";
        return $product;
    }

    public function showShoppingCart(){
        if(Session::get('ShoppingCart')!=null)
            return response()->json(['status'=>'success','ShoppingCart'=>Session::get('ShoppingCart')]);
        else
            return response()->json(['status'=>'error','No hay productos en el carrito de compras']); 
    }

    public function searchProductShowShoppingCart($product_id)
    {
        $shoppingCart=Session::get('ShoppingCart');
        for($i=0;$i<count($shoppingCart);$i++)
          if($shoppingCart[$i]['id']==$product_id)
             return $i;
        return -1;
    }

    public function deleteProductShowShoppingCart(Request $request)
    {
        $shoppingCart=Session::get('ShoppingCart');
        $position=$this->searchProductShowShoppingCart($request->product_id);
       if($position!=-1){
          unset($shoppingCart[$position]);
          $shoppingCart = array_values($shoppingCart);
          Session::put('ShoppingCart',$shoppingCart);
          return response()->json(['status'=>'success','mensaje'=>'Producto eliminado.']);
       }else{
          return response()->json(['status'=>'error','mensaje'=>'No existe el producto a eliminar.']);
       }
    }

    public function updateProductShowShoppingCart(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'ShoppingCart' => 'required|array',
        ],[
           'ShoppingCart.required'=>'Los datos a modifiar son requeridos',
           'ShoppingCart.array'=>'La estructura de los datos, es invalidad',
        ]);
        if ($validator->fails()) {
           return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
        } 

        Session::put('ShoppingCart',$request->ShoppingCart);
        return response()->json(['status'=>'success','mensaje'=>'Actualización exitosa.']);
    }

    public function destroyShowShoppingCart(Request $request)
    {
        Session::put('ShoppingCart',null);
        return response()->json(['status'=>'success','mensaje'=>'Se cancelo compra.']);
    }
    
}

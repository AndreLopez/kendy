<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductCategory;

use DB;

use Validator;

class StoreController extends Controller
{
    public function index(){
		$products = Product::all();
		return view('store.store', compact('products'));
        
    }//public function index()

	
    public function searchCategory($product_category_id) {
		$product = Product::where('product_category_id',$product_category_id)->first();
        dd($product_category_id);
    }

	public function show($slug){
		
		$product = Product::where('slug',$slug)->first();
		//dd($product);
		return view('store.show',compact('product'));
		
	}

}

<?php

namespace App\Http\Middleware;

use Closure;



class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->rol=="Administrador"){
            return $next($request);
        }// if(auth()->user()->rol=="Adminsitrador")
        return redirect('/')->with('errors','You are not allowed to this page');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable=[
       'user_id','invoice_number','method_payment','payment_code','shipping_firstname','shipping_lastname','shipping_address','shipping_city','shipping_postcode','shipping_country','subtotal','shipping'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable=[
        'sku','product_category_id','product_category_parent_id','name','slug','color_id','description','price','file','image_name','visible'
    ];

    public function productCategory(){
        return $this->belongsTo(ProductCategory::class);
    }
    

    public function productSubCategory(){
        return $this->belongsTo(ProductCategory::class,'product_category_parent_id');
    }

    public function color()
    {
        return $this->belongsTo(Color::class);
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $fillable=[
       'parent_id','name','slug','body'
    ];

    public function products(){
        return $this->hasMany(Product::class);
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_category_id')->unsigned();
            $table->integer('product_category_parent_id');
            $table->integer('color_id')->unsigned();
            $table->string('name',191);
            $table->string('slug',191);
            $table->string('sku',30);
            $table->text('description');
            $table->float('price',12,2);
            $table->longText('file')->nullable();  
            $table->text('image_name')->nullable(); 
            $table->boolean('visible')->default(true); 
            $table->timestamps();
            //RELACIONES
            $table->foreign('product_category_id')->references('id')->on('product_categories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('color_id')->references('id')->on('colors')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

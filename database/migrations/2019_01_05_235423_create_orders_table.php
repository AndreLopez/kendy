<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('invoice_number')->nullable();         
            $table->string('method_payment');
            $table->string('payment_code');
            $table->string('shipping_firstname');
            $table->string('shipping_lastname');
            $table->text('shipping_address');
            $table->string('shipping_city');
            $table->string('shipping_postcode');
            $table->string('shipping_country');
            $table->float('subtotal',12,2);
            $table->float('shipping',12,2);
            $table->string('name', 20);
            $table->string('abbreviation', 5);
            $table->string('symbol', 5);
            $table->enum('position',['RIGHT','LEFT'])->DEFAULT('LEFT');
            $table->timestamps();
            //RELACIONES
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

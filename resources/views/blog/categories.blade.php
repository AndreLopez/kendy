@extends('layouts.dashboard')
@section('subContenido')
<div class="row" id="register">
   <div class="col-md-12">
      <div class="card-box">
         <h4 class="m-t-0 header-title">GESTIÓN DE CATEGORIAS</h4>
         <p class="text-muted m-b-30 font-13">
            Los datos con<code class="highlighter-rouge"> Asterisco </code> son requeridos
         </p>
         <div class="form-row">
            <div class="form-group col-md-6">
               <label for="name" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Nombre</label>
               <input type="text" class="form-control" id="name" v-bind:class="{ 'is-invalid': nameRequired }" placeholder="Nombre" required>
            </div>
            <div class="form-group col-md-6">
               <label for="slug" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Url amigable</label>
               <input type="text" class="form-control" id="slug" v-bind:class="{ 'is-invalid': slugRequired }" placeholder="Url amigable" readonly required> 
            </div>
            <div class="form-group col-md-12">
               <label for="body" class="col-form-label">Descripción</label>
               <textarea type="text" class="form-control" id="body" placeholder="Descripción" v-model="data.body"> </textarea>
            </div>
            <div class="form-group col-md-12 text-center">
               <button type="submit" class="btn btn-warning" @click="ActionsCc">@{{buttonNameUno}}</button>
               <button type="submit" class="btn btn-primary" @click="ActionsCru">@{{buttonNameDos}}</button>
            </div>
         
            <div class="col-12 col-md-12 col-lg-12">
               <div id="tabla" v-show='categories.length'>
                  <div class="row">
                  <div class="form-group col-md-12 text-center">
                      <h4 class="text-dark  header-title m-t-0 m-b-20 pt-5">LISTADO DE CATEGORIAS</h4>
                  </div>
                     <div class="col-12 col-md-9 col-lg-9">
                        <br>
                        <div class="mt-2 form-inline font-weight-bold">
                           Mostrar
                           <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                              <option v-for="option in optionspageSize" v-bind:value="option.value">
                                 @{{ option.text }}
                              </option>
                           </select>
                           registros
                        </div>
                     </div>
                     <div class="col-12 col-md-3 col-lg-3">
                        <div class="form-group">
                           <label for="Buscar" class="font-weight-bold">Buscar:</label>
                           <input id="search" class="form-control form-control-sm"  maxlength="200"  type="text" v-model="search" v-on:keyup="filter" required>
                        </div>
                     </div>
                  </div>
                  <table  class="table table-bordered">
                     <thead class="bg-primary text-white">
                        <tr>
                           <th scope="col" style="cursor:pointer; width:45%;" class="text-center" @click="sort('name')">Nombre</th>
                           <th scope="col" style="cursor:pointer; width:45%;" class="text-center" @click="sort('slug')">Url Amigable</th>
                           <th scope="col" class="text-center width:10%;">Acciones</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr v-for="(value,index) in Registered" v-show='statusFiltro'>
                           <td class="text-justify">
                              <p class="anchoparrafo">@{{value.name}}</p>
                           </td>
                           <td class="text-justify">
                              <p class="anchoparrafo">@{{value.slug}}</p>
                           </td>
                           <td class="text-center align-middle">
                              <button class="btn btn-icon waves-effect btn-info m-b-5"  @click="captureRecord(value)" title="Editar"> <i class="fa fa-edit"></i> </button>
                              <button class="btn btn-icon waves-effect btn-danger m-b-5" title="Borrar"  @click="destroy(value)"> <i class="fa fa-trash-o"></i> </button>
                           </td>
                        </tr>
                        <tr v-show='!statusFiltro'>
                           <td class="text-justify font-weight-bold" colspan="3">No se encontraron registros</td>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <div class="">
                     <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{categories.length}}</strong>
                     <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de: 0 </strong>
                     <div style="float:right">
                        <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                        <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                        <div class="row ml-2">
                           <strong>Página:  @{{currentPage}}</strong>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         </form>
      </div>
   </div>
</div>
@endsection
@push('scripts')
<script>
   const app= new Vue({
        el:'#register',
        data:{
              data:{
                  name:'',
                  slug:'',
                  body:'',
              },
              id:'',  
              nameRequired:false,
              slugRequired:false,
              categories:{!! $categories ? $categories : "''"!!},
              buttonNameUno:"Limpiar",
              buttonNameDos:"Registrar",
              change:0,
              search:'',
              currentSort:'id',//campo por defecto que tomara para ordenar
              currentSortDir:'desc',//order asc
              pageSize:'5',//Registros por pagina
              optionspageSize: [
               { text: '5', value: 5 },
               { text: '10', value: 10 },
               { text: '25', value: 25 },
               { text: '50', value: 50 },
               { text: '100', value: 100 }
              ],//Registros por pagina
              currentPage:1,//Pagina 1
              statusFiltro:1,  
        },
        computed:{
         Registered:function() {
          this.rows=0;
          return this.categories.sort((a,b) => {
            let modifier = 1;
            if(this.currentSortDir === 'desc')
              modifier = -1;
            if(a[this.currentSort] < b[this.currentSort])
              return -1 * modifier;
            if(a[this.currentSort] > b[this.currentSort])
               return 1 * modifier;
            return 0;
           }).filter((row, index) => {
            let start = (this.currentPage-1)*this.pageSize;
            let end = this.currentPage*this.pageSize;
            if(index >= start && index < end){
               this.rows+=1;
               return true;
           }
          });
         },
        },
        methods:{
         nextPage:function() {
           if((this.currentPage*this.pageSize) < this.categories.length) this.currentPage++;
         },// nextPage:function() 
         prevPage:function() {
           if(this.currentPage > 1) this.currentPage--;
         },//prevPage:function() 
         sort:function(s) {
           //if s == current sort, reverse
           if(s === this.currentSort) {
             this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
           }
             this.currentSort = s;
         },//  sort:function(s)  
         filter:function(){
            let searched=[];
            if(this.search){
               for(let i in this.categories){
                  if(this.categories[i].name.toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.categories[i].body.toLowerCase().trim().search(this.search.toLowerCase())!=-1){
                     this.statusFiltro=1;
                     searched.push(this.categories[i]);
                  }//if(this.categories[i].name.toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.categories[i].body.toLowerCase().trim().search(this.search.toLowerCase())!=-1)
               }//for(let i in this.categories)
               if(searched.length)
                  this.categories=searched;
               else{
                  this.statusFiltro=0;
                  this.categories={!! $categories ? $categories : "''"!!};
               }// if(searched.length)
            }else{
             this.statusFiltro=1;
             this.categories={!! $categories ? $categories : "''"!!};
            }//if(this.search)
         },// filtrar:function()
         clear:function(){
            $("#name").val("");
            $("#slug").val("");
            this.data={
               name:'',
               slug:'',
               body:'',
            };  
            nameRequired=false;
            slugRequired=false;
            search='';
         },//clear:function() 
         register:function(){
            let self = this;
            var name=$("#name").val();
            var slug=$("#slug").val();
            var body=this.data.body;
            this.nameRequired=false;
            this.slugRequired=false;
            if(name==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo nombre es un campo obligatorio!');
               this.nameRequired=true;
               this.slugRequired=true;
            }//if(name=="")
            else{
               axios.post('{{ url("AddCategories") }}', {
                  name: name,
                  slug:slug,
                  body:body,
               }).then(function (response) {
                  if(response.data.status=="success"){
                     self.categories=response.data.categories;
                     self.clear();
                     $.Notification.autoHideNotify('success', 'top right', 'Información','Registro Satisfactorio.');
                  }//if(response.data.status=="success")
                  else if(response.data.status=="error"){                     
                     $.each(response.data.mensaje, function( key, value ) {
                        $.Notification.autoHideNotify('error', 'top right', 'Información',''+value);
                     });
                  }//if(response.data.status=="error")
               }).catch(function (error) {
                  console.log(error);
               });
               /*Fin envio de datos a la base de datos*/
            }//else if(name=="")   
         },//register
         captureRecord: function(value){
            var name=$("#name").val(value.name);
            var slug=$("#slug").val(value.slug);
            this.data.body=value.body;
            this.id=value.id;         
            this.nameRequired=false; 
            this.slugRequired=false;
            this.buttonNameUno="Cancelar";
            this.buttonNameDos="Actualizar";
            this.change=1;
         },
         cancel:function(){
            var name=$("#name").val("");
            var slug=$("#slug").val("");
            this.data.body="";
            this.id="";         
            this.nameRequired=false; 
            this.slugRequired=false;
            this.buttonNameUno="Limpiar";
            this.buttonNameDos="Registrar"; 
            this.change=0;
         },
         update:function(){
            let self = this;
            var name=$("#name").val();
            var slug=$("#slug").val();
            var body=this.data.body;
            var id=this.id;
            this.nameRequired=false;
            this.slugRequired=false;
            if(name==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo nombre es un campo obligatorio!');
               this.nameRequired=true;
               this.slugRequired=true;
            }//if(name=="")
            else{
               axios.post('{{ url("UpdateCategories") }}', {
                  id:id,
                  name: name,
                  slug:slug,
                  body:body,
               }).then(function (response) {
                  if(response.data.status=="success"){
                     self.categories=response.data.categories;
                     self.cancel();
                     $.Notification.autoHideNotify('success', 'top right', 'Información','Actualización Satisfactoria.');
                  }//if(response.data.status=="success")
                  else if(response.data.status=="error"){                     
                     $.each(response.data.mensaje, function( key, value ) {
                        $.Notification.autoHideNotify('error', 'top right', 'Información',''+value);
                     });
                  }//if(response.data.status=="error")
               }).catch(function (error) {
                  console.log(error);
               });
               /*Fin envio de datos a la base de datos*/
            }//else if(name=="")   
         },//update:function()
         destroy:function(value){
            let self = this;
            var id=value.id;  
            Swal({
                 title: 'Información',
                 text: "¿Esta seguro de eliminar este registro?",
                 type: 'warning',
                 showCancelButton: true,
                 confirmButtonColor: '#3085d6',
                 cancelButtonColor: '#d33',
                 confirmButtonText: '¡Sí, bórralo!!'
            }).then((result) => {
             if (result.value) {
               //
               axios.post('{{ url("DeleteCategories") }}', {id:id}).then(response => {
                  if(response.data.status=="success"){
                     self.categories=response.data.categories;
                     self.clear();
                     $.Notification.autoHideNotify('success', 'top right', 'Información','Registro eliminado con éxito.');
                  }//if(response.data.status=="success")
                  if(response.data.status=="error"){
                     self.clear();                           
                     $.each(response.data.mensaje, function( key, value ) {
                        $.Notification.autoHideNotify('error', 'top right', 'Información',''+value);
                     });
                  }//if(response.data.status=="error")
               }).catch(error => {
                         console.log(error);
               });
               //
            }
          })
         },//destroy:function()
         ActionsCru:function(){
           if(this.change==0){
             this.register();
           }//if(this.change==0)
           else if(this.change==1){
             this.update();
           }//else if(this.change==1)
         },//ActionsCru:function()
         ActionsCc:function(){
            if(this.change==0){
             this.clear();
           }//if(this.change==0)
           else if(this.change==1){
             this.cancel();
           }//else if(this.change==1)
         }//ActionsCc:function()
        },//methods
   });//const app= new Vue
</script>
<script>
   $(document).ready(function(){
     $("#name,#slug").stringToSlug({
        callback:function(text){
           $("#slug").val(text);
        }
     });
   });
</script>
@endpush


@extends('layouts.dashboard')
@section('subContenido')
<div class="row" id="register">
   <div class="col-md-12">
      <div class="card-box">
         <h4 class="m-t-0 header-title">GESTIÓN DE ENTRADAS</h4>
         <p class="text-muted m-b-30 font-13">
            Los datos con<code class="highlighter-rouge"> Asterisco </code> son requeridos
            <input id="user_id" name="user_id" type="hidden" value="{{auth()->user()->id}}">
         </p>
         <div class="form-row">
           <div class="form-group col-12 col-md-4">
               <label for="category_id" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Categorias</label>
               <select v-model="data.category_id" class="form-control" v-bind:class="{ 'is-invalid': category_idRequired }" v-bind:disabled="change==2">
                  <option value="0">Seleccione la categoria</option>
                  <option v-for="option in categories" v-bind:value="option.id">
                        @{{ option.name }}
                  </option>
               </select>
            </div>
            <div class="form-group col-12 col-md-4">
               <label for="name" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Nombre</label>
               <input type="text" class="form-control" id="name" v-bind:class="{ 'is-invalid': nameRequired }" placeholder="Nombre" v-bind:disabled="change==2" required>
            </div>
            <div class="form-group col-12 col-md-4">
               <label for="slug" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Url amigable</label>
               <input type="text" class="form-control" id="slug" v-bind:class="{ 'is-invalid': slugRequired }" placeholder="Url amigable" readonly required> 
            </div>
            <div class="form-group col-12 col-md-6 col-lg-6">
               <label for="excerpt" class="col-form-label">Extracto</label>
               <textarea type="text" class="form-control" id="excerpt" placeholder="Extracto" v-bind:class="{ 'is-invalid': excerptRequired }" v-model="data.excerpt" v-bind:disabled="change==2"> </textarea>
            </div>
            <div class="form-group col-12 col-md-6 col-lg-6">
               <label for="body" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Descripción</label>
               <textarea type="text" class="form-control" id="body" placeholder="Descripción" v-bind:class="{ 'is-invalid': bodyRequired }" v-model="data.body" v-bind:disabled="change==2"> </textarea>
            </div>
            <div class="form-group col-12">
               <label for="tags" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Etiquetas</label><br>
               <div id='tags' class="form-check-inline" v-for="option in tags">
                  <input type="checkbox" class="form-check checkbox-primary" v-bind:id="'option.id'+'tag'"  v-bind:value="option.id" v-model="data.tags" v-bind:disabled="change==2">
                  <label for="option.name" class="mt-2">@{{option.name}}</label>
               </div>
            </div>
            <div class="form-group col-12 col-md-2 col-lg-2">
               <label for="status" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Estado</label>
               <br>
               <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="status1" name="status" class="custom-control-input" value="DRAFT" v-model="data.status" v-bind:disabled="change==2">
                  <label class="custom-control-label" for="status1">Borrador</label>
               </div>
               <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="status2" name="status" class="custom-control-input" value="PUBLISHED" v-model="data.status" v-bind:disabled="change==2">
                  <label class="custom-control-label" for="status2">Publicado</label>
               </div>
            </div>
            <div class="form-group col-12 col-md-10 col-lg-10">
               <label for="image" class="col-form-label">Imagen</label><br>
               <div class="fileUpload btn btn-primary mt-2 mb-2 col-1">
                     <i class="fa fa-upload"></i>
                     <span></span>
                     <input type="file" class="upload" id="logo"  accept="image/*" v-on:change="fileImage" v-bind:disabled="change==2"/>
                  </div>
                  <strong v-if="data.image_name!=null">@{{data.image_name}}</strong>
                  <strong v-else>Selecione una imagen</strong>
            </div>
            <div class="form-group col-md-12 text-center">
               <button type="submit" class="btn btn-warning" @click="ActionsCc">@{{buttonNameUno}}</button>
               <button type="submit" class="btn btn-primary" @click="ActionsCru" v-if="buttonShowDos">@{{buttonNameDos}}</button>
            </div>
            <div class="col-12 col-md-12 col-lg-12">
               <div id="tabla" v-show='posts.length'>
                  <div class="row">
                  <div class="form-group col-md-12 text-center">
                      <h4 class="text-dark  header-title m-t-0 m-b-5 pt-5">LISTADO DE ENTRADAS</h4>
                  </div>
                     <div class="col-12 col-md-9 col-lg-9">
                        <br>
                        <div class="mt-2 form-inline font-weight-bold">
                           Mostrar
                           <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                              <option v-for="option in optionspageSize" v-bind:value="option.value">
                                 @{{ option.text }}
                              </option>
                           </select>
                           registros
                        </div>
                     </div>
                     <div class="col-12 col-md-3 col-lg-3">
                        <div class="form-group">
                           <label for="Buscar" class="font-weight-bold">Buscar:</label>
                           <input id="search" class="form-control form-control-sm"  maxlength="200"  type="text" v-model="search" v-on:keyup="filter" required>
                        </div>
                     </div>
                  </div>
                  <table  class="table table-bordered">
                     <thead class="bg-primary text-white">
                        <tr>
                           <th scope="col" style="cursor:pointer; width:44%;" class="text-center" @click="sort('name')">Nombre</th>
                           <th scope="col" style="cursor:pointer; width:44%;" class="text-center" @click="sort('slug')">Url Amigable</th>
                           <th scope="col" class="text-center width:12%;">Acciones</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr v-for="(value,index) in Registered" v-show='statusFiltro'>
                           <td class="text-justify">
                              <p class="anchoparrafo">@{{value.name}}</p>
                           </td>
                           <td class="text-justify">
                              <p class="anchoparrafo">@{{value.slug}}</p>
                           </td>
                           <td class="text-center align-middle">
                           <button class="btn btn-icon waves-effect btn-info m-b-5"  @click="captureRecordShow(value)" title="Mostrar"> <i class="fa fa-eye"></i> </button>
                              <button class="btn btn-icon waves-effect btn-primary m-b-5"  @click="captureRecord(value)" title="Editar"> <i class="fa fa-edit"></i> </button>
                              <button class="btn btn-icon waves-effect btn-danger m-b-5" title="Borrar"  @click="destroy(value)"> <i class="fa fa-trash-o"></i> </button>
                           </td>
                        </tr>
                        <tr v-show='!statusFiltro'>
                           <td class="text-justify font-weight-bold" colspan="3">No se encontraron registros</td>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <div class="">
                     <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{posts.length}}</strong>
                     <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de: 0 </strong>
                     <div style="float:right">
                        <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                        <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                        <div class="row ml-2">
                           <strong>Página:  @{{currentPage}}</strong>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         </form>
      </div>
   </div>
</div>
@endsection
@push('scripts')
<script>
   const app= new Vue({
        el:'#register',
        data:{
              data:{
                  user_id:'',
                  category_id:0,
                  name:'',
                  slug:'',
                  body:'',                  
                  excerpt:'',
                  body:'',
                  status:'DRAFT',
                  file:'',
                  image_name:'Selecione una imagen',
                  tags: []
              },
              buttonShowDos:true,
              buttonNameUno:"Limpiar",
              buttonNameDos:"Registrar",
              id:'',  
              category_idRequired:false,
              nameRequired:false,
              slugRequired:false,
              excerptRequired:false,
              bodyRequired:false,
              categories:{!! $categories ? $categories : "''"!!},
              tags:{!! $tags ? $tags : "''"!!},
              posts:{!! $posts ? $posts : "''"!!},
              change:0,
              search:'',
              currentSort:'id',//campo por defecto que tomara para ordenar
              currentSortDir:'desc',//order asc
              pageSize:'5',//Registros por pagina
              optionspageSize: [
               { text: '5', value: 5 },
               { text: '10', value: 10 },
               { text: '25', value: 25 },
               { text: '50', value: 50 },
               { text: '100', value: 100 }
              ],//Registros por pagina
              currentPage:1,//Pagina 1
              statusFiltro:1,  
        },
        computed:{
         Registered:function() {
          this.rows=0;
          return this.posts.sort((a,b) => {
            let modifier = 1;
            if(this.currentSortDir === 'desc')
              modifier = -1;
            if(a[this.currentSort] < b[this.currentSort])
              return -1 * modifier;
            if(a[this.currentSort] > b[this.currentSort])
               return 1 * modifier;
            return 0;
           }).filter((row, index) => {
            let start = (this.currentPage-1)*this.pageSize;
            let end = this.currentPage*this.pageSize;
            if(index >= start && index < end){
               this.rows+=1;
               return true;
           }
          });
         },
        },
        methods:{
         fileImage:function(event){
            // Reference to the DOM input element
            var input = event.target;
            let self = this;
            // Ensure that you have a file before attempting to read it
            if (input.files && input.files[0]) {
               // create a new FileReader to read this image and convert to base64 format
               var reader = new FileReader();
               // Define a callback function to run, when FileReader finishes its job
               reader.onload = (e) => {
                  // Note: arrow function used here, so that "this.imgLogo" refers to the imgLogo of Vue component
                  // Read image as base64 and set to imgLogo
                  self.data.file = e.target.result;
               }
               // Start the reader job - read file as a data url (base64 format)
               self.data.image_name=input.files[0].name;
               reader.readAsDataURL(input.files[0]);
            }
         },//file:function(event)  
         nextPage:function() {
           if((this.currentPage*this.pageSize) < this.posts.length) this.currentPage++;
         },// nextPage:function() 
         prevPage:function() {
           if(this.currentPage > 1) this.currentPage--;
         },//prevPage:function() 
         sort:function(s) {
           //if s == current sort, reverse
           if(s === this.currentSort) {
             this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
           }
             this.currentSort = s;
         },//  sort:function(s)  
         filter:function(){
            let searched=[];
            if(this.search){
               for(let i in this.posts){
                  if(this.posts[i].name.toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.posts[i].body.toLowerCase().trim().search(this.search.toLowerCase())!=-1){
                     this.statusFiltro=1;
                     searched.push(this.posts[i]);
                  }//if(this.posts[i].name.toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.posts[i].body.toLowerCase().trim().search(this.search.toLowerCase())!=-1)
               }//for(let i in this.posts)
               if(searched.length)
                  this.posts=searched;
               else{
                  this.statusFiltro=0;
                  this.posts={!! $posts ? $posts : "''"!!};
               }// if(searched.length)
            }else{
             this.statusFiltro=1;
             this.posts={!! $posts ? $posts : "''"!!};
            }//if(this.search)
         },// filtrar:function()
         clear:function(){
            $("#name").val("");
            $("#slug").val("");
            this.data={
               user_id:'',
               category_id:0,
               name:'',
               slug:'',
               body:'',                  
               excerpt:'',
               body:'',
               status:'DRAFT',
               file:'',
               image_name:'Selecione una imagen',
               tags: []
            }; 
            this.buttonShowDos=true;
            this.buttonNameUno="Limpiar";
            this.buttonNameDos="Registrar";
            this.category_idRequired=false;
            this.nameRequired=false;
            this.slugRequired=false;
            this.excerptRequired=false;
            this.bodyRequired=false;
            this.search='';
         },//clear:function() 
         register:function(){
            let self = this;
            var user_id=$("#user_id").val();
            var category_id=this.data.category_id;
            var name=$("#name").val();
            var slug=$("#slug").val();
            var body=this.data.body;
            var excerpt=this.data.excerpt;
            var tags=this.data.tags;
            var status=this.data.status;
            var file=this.data.file;
            var image_name=this.data.image_name;
            if(image_name=='Selecione una imagen'){
               image_name="";
            }//if(image_name=='Selecione una imagen')
            this.category_idRequired=false;
            this.nameRequired=false;
            this.slugRequired=false;
            this.excerptRequired=false;
            this.bodyRequired=false;
            if(category_id==0){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo categoria es un campo obligatorio!');
               this.category_idRequired=true;
            }//if(category_id==0)
            else if(name==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo nombre es un campo obligatorio!');
               this.nameRequired=true;
               this.slugRequired=true;
            }//else if(name=="")
            else if(excerpt==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo extracto es un campo obligatorio!');
               this.excerptRequired=true;
            }//else if(excerpt=="")
            else if(body==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo descripción es un campo obligatorio!');
               this.bodyRequired=true;
            }//else if(body=="")
            else if(tags.length==0){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo etiquetas es un campo obligatorio!');
            }//else if(tags.length==0)
            else{
               axios.post('{{ url("AddPosts") }}', {
                  user_id:user_id,
                  category_id:category_id,
                  name: name,
                  slug:slug,
                  excerpt:excerpt,
                  body:body,
                  file:file,
                  status:status,
                  tags:tags,
                  image_name:image_name,
               }).then(function (response) {
                  if(response.data.status=="success"){
                     self.clear();
                     self.posts=response.data.posts;                     
                     $.Notification.autoHideNotify('success', 'top right', 'Información','Registro Satisfactorio.');
                  }//if(response.data.status=="success")
                  else if(response.data.status=="error"){                     
                     $.each(response.data.mensaje, function( key, value ) {
                        $.Notification.autoHideNotify('error', 'top right', 'Información',''+value);
                     });
                  }//if(response.data.status=="error")
               }).catch(function (error) {
                  console.log(error);
               });
               /*Fin envio de datos a la base de datos*/
            }//else if(name=="")   
         },//register
         captureRecord: function(value){
            this.data.category_id=value.category_id;
            this.data.name=value.name;
            $("#name").val(value.name);
            this.data.slug=value.slug;
            $("#slug").val(value.slug);
            this.data.user_id=$("#user_id").val();
            this.data.excerpt=value.excerpt;
            this.data.body=value.body;
            this.data.tags=value.tags;
            this.data.status=value.status;
            this.data.file=value.file;
            this.data.image_name=value.image_name;
            this.id=value.id;         
            this.category_idRequired=false;
            this.nameRequired=false;
            this.slugRequired=false;
            this.excerptRequired=false;
            this.bodyRequired=false;
            this.buttonNameUno="Cancelar";
            this.buttonNameDos="Actualizar";
            this.change=1;
         },
         captureRecordShow: function(value){
            this.captureRecord(value);
            this.buttonShowDos=false;
            this.change=2;
         },
         cancel:function(){
            this.clear();
            this.id="";      
            this.buttonNameUno="Limpiar";
            this.buttonNameDos="Registrar";   
            this.buttonShowDos=true;
            this.change=0;
         },
         update:function(){
            let self = this;
            var user_id=$("#user_id").val();
            var category_id=this.data.category_id;
            var name=$("#name").val();
            var slug=$("#slug").val();
            var body=this.data.body;
            var excerpt=this.data.excerpt;
            var tags=this.data.tags;
            var status=this.data.status;
            var file=this.data.file;
            var image_name=this.data.image_name;
            var id=this.id;
            if(image_name=='Selecione una imagen'){
               image_name="";
            }//if(image_name=='Selecione una imagen')
            this.category_idRequired=false;
            this.nameRequired=false;
            this.slugRequired=false;
            this.excerptRequired=false;
            this.bodyRequired=false;
            if(category_id==0){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo categoria es un campo obligatorio!');
               this.category_idRequired=true;
            }//if(category_id==0)
            else if(name==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo nombre es un campo obligatorio!');
               this.nameRequired=true;
               this.slugRequired=true;
            }//else if(name=="")
            else if(excerpt==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo extracto es un campo obligatorio!');
               this.excerptRequired=true;
            }//else if(excerpt=="")
            else if(body==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo descripción es un campo obligatorio!');
               this.bodyRequired=true;
            }//else if(body=="")
            else if(tags.length==0){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo etiquetas es un campo obligatorio!');
            }//else if(tags.length==0)
            else{
               axios.post('{{ url("UpdatePosts") }}', {
                  id:id,
                  user_id:user_id,
                  category_id:category_id,
                  name: name,
                  slug:slug,
                  excerpt:excerpt,
                  body:body,
                  file:file,
                  status:status,
                  tags:tags,
                  image_name:image_name,
               }).then(function (response) {
                  if(response.data.status=="success"){
                     self.cancel();
                     self.tags=response.data.tags; 
                     self.categories=response.data.categories; 
                     self.posts=response.data.posts;    
                                              
                     $.Notification.autoHideNotify('success', 'top right', 'Información','Actualización Satisfactoria.');
                  }//if(response.data.status=="success")
                  else if(response.data.status=="error"){                     
                     $.each(response.data.mensaje, function( key, value ) {
                        $.Notification.autoHideNotify('error', 'top right', 'Información',''+value);
                     });
                  }//if(response.data.status=="error")
               }).catch(function (error) {
                  console.log(error);
               });
               /*Fin envio de datos a la base de datos*/
            }//else if(name=="")   
         },//update:function()
         destroy:function(value){
            let self = this;
            var id=value.id;  
            Swal({
                 title: 'Información',
                 text: "¿Esta seguro de eliminar este registro?",
                 type: 'warning',
                 showCancelButton: true,
                 confirmButtonColor: '#3085d6',
                 cancelButtonColor: '#d33',
                 confirmButtonText: '¡Sí, bórralo!!'
            }).then((result) => {
             if (result.value) {
               axios.post('{{ url("DeletePosts") }}', {id:id}).then(response => {
                  if(response.data.status=="success"){
                     self.tags=response.data.tags; 
                     self.categories=response.data.categories; 
                     self.posts=response.data.posts;    
                     self.clear();
                     $.Notification.autoHideNotify('success', 'top right', 'Información','Registro eliminado con éxito.');
                  }//if(response.data.status=="success")
                  if(response.data.status=="error"){
                     self.clear();                           
                     $.each(response.data.mensaje, function( key, value ) {
                        $.Notification.autoHideNotify('error', 'top right', 'Información',''+value);
                     });
                  }//if(response.data.status=="error")
               }).catch(error => {
                 console.log(error);
               });
            }//if (result.value) 
          })
         },//destroy:function()
         ActionsCru:function(){
           if(this.change==0){
             this.register();
           }//if(this.change==0)
           else if(this.change==1){
             this.update();
           }//else if(this.change==1)
         },//ActionsCru:function()
         ActionsCc:function(){
            if(this.change==0){
             this.clear();
           }//if(this.change==0)
           else if(this.change==1 || this.change==2){
             this.cancel();
           }//else if(this.change==1)
         }//ActionsCc:function()
        },//methods
   });//const app= new Vue
</script>
<script>
   $(document).ready(function(){
     $("#name,#slug").stringToSlug({
        callback:function(text){
           $("#slug").val(text);
        }
     });
   });
</script>
@endpush


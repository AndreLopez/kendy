@extends('layouts.appSub')
@section('mainContent')
@include('layouts.web.partials.navbar')
<main class="py-4">
<div class="wrapper">
            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            @yield('contentSub')
                     
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->        
        </main>
       
        @include('layouts.web.partials.footer')
@endsection
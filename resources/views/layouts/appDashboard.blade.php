<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8" />
        <title>{{ config('app.name', 'Laravel') }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">
        <link rel="stylesheet" href="{{ URL::asset('../plugins/jquery-circliful/css/jquery.circliful.css') }}">

        <!-- App css -->
        <link rel="stylesheet" href="{{ URL::asset('/css/app.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/icons.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/style.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('plugins/vamzfe/css/vamzfe.css') }}">
 
        <script src="{{ asset('assets/js/modernizr.min.js') }}"></script>
        @stack('styles')

    </head>

    <body>
 
    @yield('contenido')
     
        <!-- Plugins  -->

     
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('plugins/vamzfe/js/vamzfe.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ asset('assets/js/waves.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.nicescroll.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.scrollTo.min.js') }}"></script>
        <script src="{{ asset('../plugins/switchery/switchery.min.js') }}"></script>

         <script src="{{ asset('../plugins/notifyjs/dist/notify.min.js') }}"></script>
         <script src="{{ asset('../plugins/notifications/notify-metro.js') }}"></script>

        <script src="{{ asset('../plugins/sweet-alert/sweetalert2.all.min.js') }}"></script>

        <!-- Counter Up  -->
        <script src="{{ asset('../plugins/waypoints/lib/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('../plugins/counterup/jquery.counterup.min.js') }}"></script>
        <!-- circliful Chart -->
        <script src="{{ asset('../plugins/jquery-circliful/js/jquery.circliful.min.js') }}"></script>
        <script src="{{ asset('../plugins/jquery-sparkline/jquery.sparkline.min.js') }}"></script>

        <!-- skycons -->
        <script src="{{ asset('../plugins/skyicons/skycons.min.js') }}"></script>

        <!-- Custom main Js -->
        <script src="{{ asset('assets/js/jquery.core.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.app.js') }}"></script>
        <script src="{{ asset('../plugins/StringToSlug/jquery.stringToSlug.min.js') }}"></script>

        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('.counter').counterUp({
                    delay: 100,
                    time: 1200
                });
                $('.circliful-chart').circliful();
                
                $(".alert").fadeTo(3500, 600).slideUp(400, function () {
                 $(".alert").slideUp(600);
             });
            });

            // BEGIN SVG WEATHER ICON
            if (typeof Skycons !== 'undefined'){
                var icons = new Skycons(
                        {"color": "#3bafda"},
                        {"resizeClear": true}
                        ),
                        list  = [
                            "clear-day", "clear-night", "partly-cloudy-day",
                            "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                            "fog"
                        ],
                        i;

                for(i = list.length; i--; )
                    icons.set(list[i], list[i]);
                icons.play();
            };
        </script>
        @stack('scripts')
    </body>
</html>
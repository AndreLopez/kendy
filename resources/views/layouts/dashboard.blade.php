@extends('layouts.appDashboard')

@section('contenido')
  @include('layouts.dashboard.partials.navbar')

        <div class="wrapper">
            <div class="container-fluid">
            @if(session('info')) 
            <div class="alert alert-success alert-dismissible fade show" role="alert">
               <strong>Información!</strong>  {{session('info')}}.
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button>
            </div>
            @endif
            @if(session('warning')) 
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
               <strong>Información!</strong>  {{session('warning')}}.
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button>
            </div>
            @endif
            @if(count($errors))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
               <strong>Información!</strong><br>
               @foreach($errors->all() as $error)
                 <li>{{$error}}</li>
               @endforeach
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button>
            </div>
            @endif
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            @yield('subContenido')
                     
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

    @include('layouts.dashboard.partials.footer')
@endsection
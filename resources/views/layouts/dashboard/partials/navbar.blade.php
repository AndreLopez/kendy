

<!-- Navigation Bar-->
<header id="topnav">
   <div class="topbar-main">
      <div class="container-fluid">
         <!-- Logo container-->
         <div class="logo">
            <!-- Text Logo -->
            <a href="{{ url('dashboard') }}" class="logo">
            <span class="logo-small"><img src="{{ asset('images/fotos/kendys-logo-blanco.svg') }}" width="30" height="30" alt="kendys-logo-azul"></span>
            <span class="logo-large"><img src="{{ asset('images/fotos/kendys-logo-blanco.svg') }}" width="30" height="30" alt="kendys-logo-azul"> {{ config('app.name', 'Laravel') }}</span>
            </a>
         </div>
         <!-- End Logo container-->
         <div class="menu-extras topbar-custom">
            <ul class="list-inline float-right mb-0">
               <li class="menu-item list-inline-item">
                  <!-- Mobile menu toggle-->
                  <a class="navbar-toggle nav-link">
                     <div class="lines">
                        <span></span>
                        <span></span>
                        <span></span>
                     </div>
                  </a>
                  <!-- End mobile menu toggle-->
               </li>
               {{--<li class="list-inline-item dropdown notification-list">
                  <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button"
                     aria-haspopup="false" aria-expanded="false">
                  <i class="mdi mdi-bell noti-icon"></i>
                  <span class="badge badge-pink noti-icon-badge">4</span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg" aria-labelledby="Preview">
                     <!-- item-->
                     <div class="dropdown-item noti-title">
                        <h5 class="font-16"><span class="badge badge-danger float-right">5</span>Notificaciones</h5>
                     </div>
                     <!-- item-->
                     <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <div class="notify-icon bg-success"><i class="mdi mdi-comment-account"></i></div>
                        <p class="notify-details"><small class="text-muted"></small></p>
                     </a>
                     <!-- item-->
                     <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <div class="notify-icon bg-info"><i class="mdi mdi-account"></i></div>
                        <p class="notify-details"><small class="text-muted"></small></p>
                     </a>
                     <!-- item-->
                     <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <div class="notify-icon bg-danger"><i class="mdi mdi-airplane"></i></div>
                        <p class="notify-details"><b></b><small class="text-muted"></small></p>
                     </a>
                     <!-- All-->
                     <a href="javascript:void(0);" class="dropdown-item notify-item notify-all">
                     Ver más
                     </a>
                  </div>
               </li>--}}
               <li class="list-inline-item dropdown notification-list">
               <a class="nav-link dropdown-toggle waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                     aria-haspopup="false" aria-expanded="false">Bienvenido! {{ Auth::user()->name }}</a>
               </li>   
               <li class="list-inline-item dropdown notification-list">
                  <a class="nav-link dropdown-toggle waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                     aria-haspopup="false" aria-expanded="false">
                  <img src="assets/images/users/avatar-1.png" alt="user" class="rounded-circle">
                  </a>
                  <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                     <!-- item-->
                     <div class="dropdown-item noti-title">
                        <h5 class="text-overflow"><small class="text-white"><strong>Bienvenido! {{ Auth::user()->name }}</strong> </small> </h5>
                     </div>
                     <!-- item-->
                     <a href="{{ url('profile') }}" class="dropdown-item notify-item">
                     <i class="mdi mdi-account"></i> <span>Perfil</span>
                     </a>
                     <!-- item-->
                     <a href="{{ url('users') }}" class="dropdown-item notify-item">
                     <i class="mdi mdi-settings"></i> <span>Ajustes</span>
                     </a>
                     <!-- item-->
                     <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                     <i class="mdi mdi-logout"></i> <span>Salir</span>
                     </a>
                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                     </form>
                  </div>
               </li>
            </ul>
         </div>
         <!-- end menu-extras -->
         <div class="clearfix"></div>
      </div>
      <!-- end container -->
   </div>
   <!-- end topbar-main -->
   <div class="navbar-custom">
      <div class="container-fluid">
         <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">
               <li class="has-submenu">
                  <a href="{{ url('dashboard') }}"><i class="ti-home"></i>Dashboard</a>
               </li>
               <li class="has-submenu">
                  <a href="#"><i class="ti-shine"></i>Ajustes</a>
                  <ul class="submenu">
                     <li><a href="{{ url('users') }}">Usuarios</a></li>
                  </ul>
               </li>
               <li class="has-submenu">
                  <a href="#"><i class="ti-shopping-cart-full"></i>Tienda</a>
                  <ul class="submenu">
                     <li><a href="{{ url('coins') }}">Monedas</a></li>
                     <li><a href="{{ url('productsColors') }}">Colores</a></li>
                     <li><a href="{{ url('productsCategories') }}">Categorias</a></li>
                     <li><a href="{{ url('products') }}">Productos</a></li>
                     <li><a href="{{ url('orders') }}">Ordenes</a></li>
                  </ul>
               </li>
               <li class="has-submenu">
                  <a href="#"><i class="ti-tag"></i>Blog</a>
                  <ul class="submenu">
                     <li><a href="{{ url('tags') }}">Etiquetas</a></li>
                     <li><a href="{{ url('categories') }}">Categorias</a></li>
                     <li><a href="{{ url('posts') }}">Entradas</a></li>
                  </ul>
               </li>
            </ul>
            <!-- End navigation menu -->
         </div>
         <!-- end #navigation -->
      </div>
      <!-- end container -->
   </div>
   <!-- end navbar-custom -->
</header>
<!-- End Navigation Bar-->


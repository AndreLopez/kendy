{{-- Hojas de estilos --}}
	<link rel="stylesheet" href="{{ asset('css/kendysstore.css') }}">
	<link rel="stylesheet" href="{{ asset('fontawesome-free-5.5.0-web/css/all.css') }}">

{{-- Favicon --}}
<link rel="icon" sizes="any" href="{{ asset('images/favicon.png') }}">

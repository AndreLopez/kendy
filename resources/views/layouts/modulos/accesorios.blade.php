<section class="accessories sections__accessories" id="accesorios">
	
	<div class="text-content accessories--text-content accessories--background-image">
		<h2 class="kendys--title">Accesorios</h2>
		<p class="text-content__p text--small fc--white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum quisquam ad non! Pariatur, mollitia repudiandae veniam. Nobis ut architecto nisi fuga est suscipit consequatur non voluptate! Natus, dolore assumenda! Obcaecati.</p>
	</div>

	<div class="accessories--content">
		{{-- Rejillas --}}
		<div class="grid column-4">
			<div class="grid--item grid--gallery far fa-image"><img class="grid--image" src="{{ asset('images/fotos/IMG_0807.jpg') }}" alt=" "></div>
			<div class="grid--item grid--gallery far fa-image"><img class="grid--image" src="{{ asset('images/fotos/IMG_0816.jpg') }}" alt=" "></div>
			<div class="grid--item grid--gallery far fa-image"><img class="grid--image" src="{{ asset('images/fotos/IMG_0806.jpg') }}" alt=" "></div>
			<div class="grid--item grid--gallery far fa-image"><img class="grid--image" src="{{ asset('images/fotos/') }}" alt=" "></div>
			<div class="grid--item grid--gallery far fa-image"><img class="grid--image" src="{{ asset('images/fotos/IMG_0816.jpg') }}" alt=""></div>
			<div class="grid--item grid--gallery far fa-image"><img class="grid--image" src="{{ asset('images/fotos/') }}" alt=" "></div>
			<div class="grid--item grid--gallery far fa-image"><img class="grid--image" src="{{ asset('images/fotos/') }}" alt=" "></div>
			<div class="grid--item grid--gallery far fa-image"><img class="grid--image" src="{{ asset('images/fotos/IMG_0816.jpg') }}" alt=" "></div>
		</div>
	</div>

</section>
<section class="blog sections__blog" id="blog">
	<div class="text-content blog--text-content text--uppercase flex justify-content--center">
		Blog
	</div>

	<div class="blog--content-card grid column-4 gap-20">
		<div class="item blog--card">
			<div class="blog--img">
				<img src="{{ asset('images/fotos/IMG_0816-p.jpg') }}" alt="Foto">
			</div>

			<div class="blog--text">
				<h2>Title</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error laborum qui molestias, doloremque ea praesentium iste eveniet quibusdam excepturi atque laboriosam tempore aliquid repellendus, voluptate fuga. Unde voluptatum modi hic?</p>
				<strong class="block strong-lg">Fabio Contreras</strong>
				<a href="#" class="item--link"></a>
			</div>
		</div>

		<div class="item blog--card">
			<div class="blog--img">
				<img src="{{ asset('images/fotos/DSC_6096-p.jpg') }}" alt="Foto">
			</div>

			<div class="blog--text">
				<h2>Title</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore sint minus autem fugit reprehenderit unde! Assumenda perspiciatis, culpa necessitatibus sed ad facere maiores minus odit explicabo aspernatur quos quisquam quas?</p>
				<strong class="block strong-lg">Armanis de la Hoz</strong>
				<a href="#" class="item--link"></a>
			</div>
		</div>

		<div class="item blog--card">
			<div class="blog--img">
				<img src="{{ asset('images/fotos/DSC_6212-p.jpg') }}" alt="Foto">
			</div>

			<div class="blog--text">
				<h2>Title</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum possimus, quasi perferendis esse deserunt numquam pariatur unde! Accusamus architecto consectetur velit sequi, tempora corrupti optio fugit voluptatibus magnam ullam repellat?</p>
				<strong class="block strong-lg">Carol Herna</strong>
				<a href="#" class="item--link"></a>
			</div>
		</div>

		<div class="item blog--card">
			<div class="blog--img">
				<img src="{{ asset('images/fotos/DSC_6144-p.jpg') }}" alt="Foto">
			</div>

			<div class="blog--text">
				<h2>Title</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur molestiae aliquam illum. Quaerat debitis illum quasi cupiditate voluptate aspernatur, enim magnam reiciendis, quidem ea quae iusto fugiat sequi ex corrupti.</p>
				<strong class="block strong-lg">Carol Herna</strong>
				<a href="#" class="item--link"></a>
			</div>
		</div>
	</div>
</section>
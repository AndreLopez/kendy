<section class="collections sections__collections" id="colecciones">
	<div class="text-content">
		<div class="collections--tips">
			<h2 class="text--big text--uppercase fc-gray-90">Colecciones</h2>
			<p class="text--small fc-gray-90">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium cupiditate dolorem veniam, laboriosam laborum ab hic placeat! Nisi in vel, earum sequi illo, distinctio! Maiores obcaecati unde iure nemo a.</p>
		</div>
	</div>

	<div class="grid column-3">
		<div class="grid--item "><img src="{{ asset('images/fotos/FOTO-1.jpg') }}" alt="Foto" class="grid--image"></div>

		<div class="collections--item grid--item  bc--gray-90">
			<h2 class="collections--h text-lg margin-bottom--10px">Vodel Model</h2>
			<hr>
			<p class="fc--white margin-bottom--30px margin-top--20px">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus nobis dolorem repellendus ea adipisci. Ut laudantium, at eius consequatur tempora, illum id ipsum veritatis labore est aperiam, consequuntur aspernatur placeat!</p>

			<h3 class="text--big text--uppercase">Autor de la vestimenta</h3>
		</div>

		<div class="grid--item "><img src="{{ asset('images/fotos/FOTO-2.jpg') }}" alt="Foto" class="grid--image"></div>

		<div class="collections--item grid--item  bc--gray-90">
			<h2 class="collections--h text-lg margin-bottom--10px">Vodel Model Model</h2>
			<hr>
			<p class="fc--white margin-bottom--30px margin-top--20px">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non consequatur in adipisci labore dignissimos suscipit facilis dolores, nesciunt maiores aperiam iste voluptatum, sit, porro libero dolor. Reiciendis est nisi placeat!</p>

			<h3 class="text--big text--uppercase">Autor de la Vestimenta</h3>
		</div>

		<div class="grid--item "><img src="{{ asset('images/fotos/FOTO-3.jpg') }}" alt="Foto" class="grid--image"></div>

		<div class="collections--item grid--item  bc--gray-90">
			<h2 class="collections--h text-lg margin-bottom--10px">Vodel Model</h2>
			<hr>

			<p class="fc--white margin-bottom--30px margin-top--20px">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea, nesciunt laborum recusandae tenetur fugit deserunt aspernatur facere mollitia quae? Incidunt nisi fugiat atque aliquid! Maxime iure officiis doloribus natus similique!</p>

			<h3 class="text--big text--uppercase">Autor de la vestimenta</h3>
		</div>
	</div>
</section>
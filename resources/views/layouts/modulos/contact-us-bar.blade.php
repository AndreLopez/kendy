<nav class="navbar-contact-us flex align-items--center justify-content--center flex--wrap" id="contact-us">
	<form action="" class="none" id="form-email"></form>
	<h3 class="h navbar-contact-us--item">CONTACTOS</h3>

	{{-- Formulario de búsqueda --}}
	<div class="input-text navbar-contact-us--item">
		<input type="text" name="email" for="form-email" placeholder="correo@dominio.com" />
		<button type="submit" for="email">&gt;</button>
	</div>

	{{-- Teléfonos --}}
	<div class="phone navbar-contact-us--item">
		<img class="navbar-contact-us--icons" src="{{ asset('images/iconos/phone.svg') }}" alt="Phone">
		<span>+58 658 4548</span>
		<span>(0261) 725 2136</span>
	</div>

	{{-- Correo electrónico --}}
	<div class="email navbar-contact-us--item">
		<img class="navbar-contact-us--icons" src="{{ asset('images/iconos/email.svg') }}" alt="Email">
		<span>contactokendysve@gmail.com</span>
	</div>

	{{-- Redes Sociales --}}
	<div class="rrss navbar-contact-us--item"></a>
		<a href="#" class="navbar-contact-us--link"><span class="fab fa-instagram fa-lg"></span></a>
		<a href="#" class="navbar-contact-us--link"><span class="fab fa-twitter fa-lg"></span></a>
		<a href="#" class="navbar-contact-us--link"><span class="fab fa-facebook-f fa-lg"></span></a>
		<a href="#" class="navbar-contact-us--link"><span>@kendys_ve</span></a>
	</div>
</nav>
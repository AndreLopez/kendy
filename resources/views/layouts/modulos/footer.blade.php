<footer class="footer main__footer width--100">
	{{-- @include('layouts.modulos.contact-us-bar') --}}

	<div class="flex flex--wrap align-items--center footer--border footer--padding">
		<div class="flex--item flex--grow-3 flex justify-content--center">
				{{-- code --}}
				<div class="content-image">
					<div class="content-image--image flex justify-content--center align-items--center">
						<img src="{{ asset('images/fotos/kendys-logo.svg') }}" alt="Kendys">
					</div>

					<div class="content-image--caption flex justify-content--center align-items--center text--center">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
					</div>
				</div>
		</div>

		<div class="links footer--item flex--grow-1 flex--shrink-1 flex--item">
				<h2 class="links--title">COLECCIONES</h2>
				<ul class="links--menu">
					<li class="links--item"><a class="links--link" href="#">PRIMAVERA</a></li>
					<li class="links--item"><a class="links--link" href="#">OTOÑO</a></li>
					<li class="links--item"><a class="links--link" href="#">CLASIC GIRL</a></li>
					<li class="links--item"><a class="links--link" href="#">CASUAL GIRL</a></li>
					<li class="links--item"><a class="links--link" href="#">ROSADO PROFUNDO</a></li>
				</ul>
		</div>

		<div class="links footer--item flex--grow-1 flex--shrink-1 flex--item">
				<h2 class="links--title">SOBRE NOSOTROS</h2>
				<ul class="links--menu">
					<li class="links--item"><a class="links--link" href="#">HISTORIA</a></li>
					<li class="links--item"><a class="links--link" href="#">MISIÓN</a></li>
					<li class="links--item"><a class="links--link" href="#">VISIÓN</a></li>
					<li class="links--item"><a class="links--link" href="#">PROPÓSITO</a></li>
					<li class="links--item"><a class="links--link" href="#">VALORES</a></li>
				</ul>
		</div>

		<div class="links footer--item flex--grow-1 flex--shrink-1 flex--item">
				<h2 class="links--title">NUESTRO BLOG</h2>
				<ul class="links--menu">
					<li class="links--item"><a class="links--link" href="#">COLECCIONES</a></li>
					<li class="links--item"><a class="links--link" href="#">TIPS KENDYS</a></li>
					<li class="links--item"><a class="links--link" href="#">CLASSIC GIRL</a></li>
					<li class="links--item"><a class="links--link" href="#">NUESTRA FUNDADORA</a></li>
					<li class="links--item"><a class="links--link" href="#">OTROS</a></li>
				</ul>
		</div>

	</div>

	<div class="info flex justify-content--center align-items--center">
		<span class="info--item">&copy; - COPYRIGHT 2019</span>
		<span class="info--item"><a class="info--link" href="#">POLÍTICA DE PRIVACIDAD</a></span>
		<span class="info--item"><a class="info--link" href="#">DERECHOS RESERVADOS</a></span>
	</div>

	<div class="website flex justify-content--center align-items--center">
		www.kendys.com.ve
	</div>
</footer>

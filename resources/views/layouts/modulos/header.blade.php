<header class="header main__header width--100">
	@include('layouts.modulos.navbar')
	
	<div class="banner header__banner">
		<div class="images banner__images">
			<img class="banner__img" src="{{ asset('images/fotos/banners-portada-p.jpg') }}" alt="Fotos de banner">
			{{-- <img bannes__img src="{{ asset('images/fotos/foto.jpg') }}" alt="Fotos de banner">
			<img class="banner__img" src="{{ asset('images/fotos/foto.jpg') }}" alt="Fotos de banner">
			<img class="banner__img" src="{{ asset('images/fotos/foto.jpg') }}" alt=""Fotos de banner> --}}
		</div>

		{{-- Indicadores de imagen --}}
		<div class="indicator banner--indicator">
			<div class="indicator--item indicator--selected"></div>
			<div class="indicator--item"></div>
			<div class="indicator--item"></div>
			<div class="indicator--item"></div>
		</div>

		{{-- Navegador entre imágenes --}}
		<div class="nav-image--left fas fa-chevron-left"></div>
		<div class="nav-image--right fas fa-chevron-right"></div>
	</div>

</header>
<nav class="navbar header__navbar">
		{{-- Logotipo de Kendys --}}
		<div class="navbar__logo">
			<img class="navbar__logo--img" src="{{ asset('images/fotos/kendys-logo.svg') }}" alt="Logotipo">
		</div>
		
		
		{{-- Toggle Menú --}}
		<input class="navbar__checkbox" type="checkbox" id="chk-menu">
		<label class="navbar__label" for="chk-menu"><span class="fas fa-bars fa-lg"></span></label>
		
		{{-- Menú de navegación --}}
		<ul class="menu navbar__menu">
			<li class="menu__item navbar__menu--item"><a class="menu__link navbar__menu--link" href="">Inicio</a></li>
			<li class="menu__item navbar__menu--item"><a class="menu__link navbar__menu--link" href="#nosotros">Nosotros</a></li>
			<li class="menu__item navbar__menu--item"><a class="menu__link navbar__menu--link" href="#colecciones">Colecciones</a></li>
			<li class="menu__item navbar__menu--item"><a class="menu__link navbar__menu--link" href="#accesorios">Accesorios</a></li>
			<li class="menu__item navbar__menu--item"><a class="menu__link navbar__menu--link" href="{{ route('store') }}">Shopping</a></li>
			<li class="menu__item navbar__menu--item"><a class="menu__link navbar__menu--link" href="#blog">Blog</a></li>
			<li class="menu__item navbar__menu--item"><a class="menu__link navbar__menu--link" href="#contact-us">Contacto</a></li>
		</ul>
	</nav>
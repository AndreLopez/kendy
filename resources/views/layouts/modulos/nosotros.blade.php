<section class="about sections__about flex flex--column" id="nosotros">
	{{-- Bloque de cabecera --}}
	<div class="text-content">
		{{-- Ofertas --}}
		<div class="about--tips">
			<span>Si te perdiste nuestra última colección visítamos</span>
			<div class="about--buttons">
				<a href="#" class="button button--solid-translucid button--uppercase button--small-t">Click Aquí</a>
			</div>
		</div>

		{{-- Título --}}
		<div class="about--title">
			<h1 class="text--big text--uppercase fc--base">Sobre Nosotros</h1>
		</div>

		<div class="about--description">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt consequatur autem, voluptas inventore accusantium soluta assumenda accusamus, porro temporibus ab modi eum obcaecati saepe quidem perspiciatis. Aperiam alias quam laudantium?
		</div>

	</div>
	
	{{-- Slider --}}
	<div class="sliders about--sliders">
		<div class="sliders--content flex">
			{{-- Bloque izquierdo --}}
			<div class="sliders--left bc--gray-90 flex flex--column justify-content--center align-items--center">
				<h2 class="text--uppercase">Nuestra Historia</h2>
				<p class="width--70 text--center fc--white text--small">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, omnis labore quod eos nisi, ducimus explicabo accusamus, laborum nihil nam tempora libero error quas adipisci cum eum consequatur quae ab!</p>
	
				<a href="#" class="button button--uppercase button--solid-translucid fc--white">Historia</a>
			</div>
	
			{{-- Bloque derecho --}}
			<div class="sliders--right flex--grow-2">
				{{-- Imagen de la columna derecha --}}
				<div class="images bc--black">
					<img src="{{ asset('images/fotos/foto-historia-80.jpg') }}" alt="Foto">
				</div>
	
				{{-- Texto de la imagen de la columna derecha --}}
				<div class="column--text">
					<div class="text-caption">
						<h2 class="text--uppercase">La arquitectura es como la moda</h2>
						<p class="fc--gray-50">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius molestiae incidunt, iusto delectus sequi veritatis consequuntur dicta, cupiditate qui blanditiis voluptate facilis ab ipsa suscipit non voluptates! Similique, aliquam, quos.</p>
					</div>
				</div>
			</div>
		</div>

		{{-- Indicadores --}}
		<div class="indicator">
			<div class="indicator--item indicator--selected"></div>
			<div class="indicator--item"></div>
			<div class="indicator--item"></div>
			<div class="indicator--item"></div>
		</div>
	</div>
	

</section>
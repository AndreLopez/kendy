<!-- Footer -->
<footer class="footer">
   <div class="container">
      <div class="row">
         <div class="col-12 text-center">
            2018 © {{ config('app.name', 'Laravel') }}
         </div>
      </div>
   </div>
</footer>
<!-- End Footer -->


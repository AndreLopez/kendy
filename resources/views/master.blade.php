<!DOCTYPE html>
<html lang="es-Es">
<head>
	<meta charset="UTF-8">
	<title>Kendys · HOME</title>

	{{-- Hojas de estilos --}}
	@include('layouts.modulos.head')

</head>
<body>
	<main role="main" class="main flex flex--wrap justify-content--center">
		{{-- Cabecera --}}
		@include('layouts.modulos.header')

		{{-- Secciones --}}
		<div class="sections main__sections">
			@include('layouts.modulos.nosotros')
			@include('layouts.modulos.colecciones')
			@include('layouts.modulos.accesorios')
			@include('layouts.modulos.blog')
		</div>

		{{-- Barra de navegación  --}}
		@include('layouts.modulos.contact-us-bar')

		{{-- Pie de página --}}
		@include('layouts.modulos.footer')
	</main>
</body>
</html>

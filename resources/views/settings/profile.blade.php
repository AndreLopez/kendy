@extends('layouts.dashboard')
@section('subContenido')
<div class="row" id="register">
   <div class="col-md-12">
      <div class="card-box">
         <h4 class="m-t-0 header-title">Perfil</h4>
         <p class="text-muted m-b-30 font-13">
            Los datos con<code class="highlighter-rouge"> Asterisco </code> son requeridos
         </p>
         <div class="form-row">
            <div class="form-group col-md-6">
               <label for="name" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Nombres</label>
               <input type="text" class="form-control" id="name" v-bind:class="{ 'is-invalid': nameRequired }" v-model="data.name" placeholder="Nombre" maxlength="50" required>
            </div>
            <div class="form-group col-md-6">
               <label for="email" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Correo Electrónico</label>
               <input type="text" class="form-control" id="email" v-bind:class="{ 'is-invalid': emailRequired }" v-model="data.email" placeholder="Correo Electrónico" maxlength="50" readonly required>
            </div>
            <div class="form-group col-12 col-md-4">
               <label for="rol" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Rol</label>
               <select v-model="data.rol" class="form-control" v-bind:class="{ 'is-invalid': rolRequired }">
                  <option value="0">Seleccione el rol</option>
                  <option value="Administrador">Administrador</option>
                  <option value="Usuario">Usuario</option>
               </select>
            </div>
            <div class="form-group col-md-4">
               <label for="password" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Contraseña</label>
               <input type="password" class="form-control" id="password" v-bind:class="{ 'is-invalid': passwordRequired }" v-model="data.password" placeholder="Contraseña" maxlength="20" required>
            </div>
            <div class="form-group col-md-4">
               <label for="passwordConfirmation" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Confirmar Contraseña</label>
               <input type="password" class="form-control" id="passwordConfirmation" v-bind:class="{ 'is-invalid': passwordConfirmationRequired }" v-model="data.passwordConfirmation" maxlength="20" placeholder="Confirmar Contraseña" required>
            </div>
            <div class="form-group col-md-12 text-center">
               <button type="submit" class="btn btn-primary" @click="update">@{{buttonNameDos}}</button>
            </div>
         </div>
         </form>
      </div>
   </div>
</div>
@endsection
@push('scripts')
<script>
   const app= new Vue({
        el:'#register',
        data:{
              data:{
                  name:'',
                  email:'',
                  password:'',
                  passwordConfirmation:'',
                  rol:0,
              },
              id:'',  
              nameRequired:false,
              emailRequired:false,
              passwordRequired:false,
              passwordConfirmationRequired:false,
              rolRequired:false,
              buttonNameDos:"Actualizar",
              change:1,
        },
        mounted(){
          this.load();
        },
        methods:{
         validEmail: function (email) {
           var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
           return re.test(email);
         },//validEmail
         load:function(){
            let self = this;
            axios.get('{{ url("loadUser") }}', {}).then(function (response) {
                self.id=response.data.user.id;
                self.data.name=response.data.user.name;
                self.data.email=response.data.user.email;
                self.data.rol=response.data.user.rol;
               }).catch(function (error) {
                  console.log(error);
               });
         },//load:function()
         update:function(){
            let self = this;
            var id=this.id;
            var name=this.data.name;
            var email=this.data.email;
            var password=this.data.password;
            var passwordConfirmation=this.data.passwordConfirmation;
            var rol=this.data.rol;
            this.nameRequired=false;
            this.emailRequired=false;
            this.passwordRequired=false;
            this.passwordConfirmationRequired=false;
            this.rolRequired=false;
            var flag=0;
            if(name==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo nombre es un campo obligatorio!');
               this.nameRequired=true;
               flag=1;
            }//if(name=="")
            else if(name.length<3){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo nombre acepta un minimo de 3 caracteres');
               this.nameRequired=true;
               flag=1;
            }//else if(name.length<3)
            else if(email==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo correo electrónico es un campo obligatorio!');
               this.emailRequired=true;
               flag=1;
            }//else if(email=="")
            else if(!this.validEmail(email)){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El correo electrónico  introducido es invalido!');
               this.emailRequired=true;
               flag=1;
            }//else if(!this.validEmail(email))
            else if(rol==0){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo rol es un campo obligatorio!');
               this.rolRequired=true;
               flag=1;
            }//if(name=="")
            else if(password!=""){
               if(password.length<6){
                  $.Notification.autoHideNotify('error', 'top right', 'Información','El campo contraseña acepta un minimo de 6 caracteres!');
                  this.passwordRequired=true;
                  flag=1;
               }//else if(password.length<6)
               else if(passwordConfirmation==""){
                  $.Notification.autoHideNotify('error', 'top right', 'Información','El campo verificación de la contraseña es un campo obligatorio!');
                  this.passwordConfirmationRequired=true;
                  flag=1;
               }//else if(passwordConfirmation=="")
               else if(passwordConfirmation.length<6){
                  $.Notification.autoHideNotify('error', 'top right', 'Información','El campo verificación de la contraseña acepta un minimo de 6 caracteres!');
                  this.passwordConfirmationRequired=true;
                  flag=1;
               }//else if(passwordConfirmation.length<6)
               else if(password!=passwordConfirmation){
                  $.Notification.autoHideNotify('error', 'top right', 'Información','El campo verificación de la contraseña acepta un minimo de 6 caracteres!');
                  this.passwordRequired=true;
                  this.passwordConfirmationRequired=true;
                  flag=1;
               }//else if(passwordConfirmation.length<6)  
            }//else if(password!="")
            if(flag==0){
               axios.post('{{ url("UpdateUsers") }}', {
                  id:id,
                  name:name,
                  email:email,
                  password:password,
                  passwordConfirmation:passwordConfirmation,
                  rol:rol,
               }).then(function (response) {
                  if(response.data.status=="success"){
                     self.users=response.data.users;
                     self.cancel();
                     $.Notification.autoHideNotify('success', 'top right', 'Información','Actualización Satisfactoria.');
                  }//if(response.data.status=="success")
                  else if(response.data.status=="error"){                     
                     $.each(response.data.mensaje, function( key, value ) {
                        $.Notification.autoHideNotify('error', 'top right', 'Información',''+value);
                     });
                  }//if(response.data.status=="error")
               }).catch(function (error) {
                  console.log(error);
               });
               /*Fin envio de datos a la base de datos*/
            }//else if(name=="")   
         },//update:function()
        },//methods
   });//const app= new Vue
</script>
@endpush


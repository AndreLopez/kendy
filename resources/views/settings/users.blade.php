@extends('layouts.dashboard')
@section('subContenido')
<div class="row" id="register">
   <div class="col-md-12">
      <div class="card-box">
         <h4 class="m-t-0 header-title">GESTIÓN DE USUARIOS</h4>
         <p class="text-muted m-b-30 font-13">
            Los datos con<code class="highlighter-rouge"> Asterisco </code> son requeridos
         </p>
         <div class="form-row">
            <div class="form-group col-md-6">
               <label for="name" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Nombres</label>
               <input type="text" class="form-control" id="name" v-bind:class="{ 'is-invalid': nameRequired }" v-model="data.name" placeholder="Nombre" maxlength="50" required>
            </div>
            <div class="form-group col-md-6">
               <label for="email" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Correo Electrónico</label>
               <input type="text" class="form-control" id="email" v-bind:class="{ 'is-invalid': emailRequired }" v-model="data.email" placeholder="Correo Electrónico" maxlength="50" required>
            </div>
            <div class="form-group col-12 col-md-4">
               <label for="rol" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Rol</label>
               <select v-model="data.rol" class="form-control" v-bind:class="{ 'is-invalid': rolRequired }">
                  <option value="0">Seleccione el rol</option>
                  <option value="Administrador">Administrador</option>
                  <option value="Usuario">Usuario</option>
               </select>
            </div>
            <div class="form-group col-md-4">
               <label for="password" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Contraseña</label>
               <input type="password" class="form-control" id="password" v-bind:class="{ 'is-invalid': passwordRequired }" v-model="data.password" placeholder="Contraseña" maxlength="20" required>
            </div>
            <div class="form-group col-md-4">
               <label for="passwordConfirmation" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Confirmar Contraseña</label>
               <input type="password" class="form-control" id="passwordConfirmation" v-bind:class="{ 'is-invalid': passwordConfirmationRequired }" v-model="data.passwordConfirmation" maxlength="20" placeholder="Confirmar Contraseña" required>
            </div>
            <div class="form-group col-md-12 text-center">
               <button type="submit" class="btn btn-warning" @click="ActionsCc">@{{buttonNameUno}}</button>
               <button type="submit" class="btn btn-primary" @click="ActionsCru">@{{buttonNameDos}}</button>
            </div>
         
            <div class="col-12 col-md-12 col-lg-12">
               <div id="tabla" v-show='users.length'>
                  <div class="row">
                  <div class="form-group col-md-12 text-center">
                      <h4 class="text-dark  header-title m-t-0 m-b-20 pt-5">LISTADO DE USUARIOS</h4>
                  </div>
                     <div class="col-12 col-md-9 col-lg-9">
                        <br>
                        <div class="mt-2 form-inline font-weight-bold">
                           Mostrar
                           <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                              <option v-for="option in optionspageSize" v-bind:value="option.value">
                                 @{{ option.text }}
                              </option>
                           </select>
                           registros
                        </div>
                     </div>
                     <div class="col-12 col-md-3 col-lg-3">
                        <div class="form-group">
                           <label for="Buscar" class="font-weight-bold">Buscar:</label>
                           <input id="search" class="form-control form-control-sm"  maxlength="200"  type="text" v-model="search" v-on:keyup="filter" required>
                        </div>
                     </div>
                  </div>
                  <table  class="table table-bordered">
                     <thead class="bg-primary text-white">
                        <tr>
                           <th scope="col" style="cursor:pointer; width:30%;" class="text-center" @click="sort('name')">Nombre</th>
                           <th scope="col" style="cursor:pointer; width:30%;" class="text-center" @click="sort('email')">Correo Electrónico</th>
                           <th scope="col" style="cursor:pointer; width:30%;" class="text-center" @click="sort('rol')">Rol</th>
                           <th scope="col" class="text-center width:10%;">Acciones</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr v-for="(value,index) in Registered" v-show='statusFiltro'>
                           <td class="text-justify">
                              <p class="anchoparrafo">@{{value.name}}</p>
                           </td>
                           <td class="text-justify">
                              <p class="anchoparrafo">@{{value.email}}</p>
                           </td>
                           <td class="text-justify">
                              <p class="anchoparrafo">@{{value.rol}}</p>
                           </td>
                           <td class="text-center align-middle">
                              <button class="btn btn-icon waves-effect btn-info m-b-5"  @click="captureRecord(value)" title="Editar"> <i class="fa fa-edit"></i> </button>
                              <button class="btn btn-icon waves-effect btn-danger m-b-5" title="Borrar"  @click="destroy(value)"> <i class="fa fa-trash-o"></i> </button>
                           </td>
                        </tr>
                        <tr v-show='!statusFiltro'>
                           <td class="text-justify font-weight-bold" colspan="3">No se encontraron registros</td>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <div class="">
                     <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{users.length}}</strong>
                     <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de: 0 </strong>
                     <div style="float:right">
                        <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                        <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                        <div class="row ml-2">
                           <strong>Página:  @{{currentPage}}</strong>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         </form>
      </div>
   </div>
</div>
@endsection
@push('scripts')
<script>
   const app= new Vue({
        el:'#register',
        data:{
              data:{
                  name:'',
                  email:'',
                  password:'',
                  passwordConfirmation:'',
                  rol:0,
              },
              id:'',  
              nameRequired:false,
              emailRequired:false,
              passwordRequired:false,
              passwordConfirmationRequired:false,
              rolRequired:false,
              users:{!! $users ? $users : "''"!!},
              buttonNameUno:"Limpiar",
              buttonNameDos:"Registrar",
              change:0,
              search:'',
              currentSort:'id',//campo por defecto que tomara para ordenar
              currentSortDir:'desc',//order asc
              pageSize:'5',//Registros por pagina
              optionspageSize: [
               { text: '5', value: 5 },
               { text: '10', value: 10 },
               { text: '25', value: 25 },
               { text: '50', value: 50 },
               { text: '100', value: 100 }
              ],//Registros por pagina
              currentPage:1,//Pagina 1
              statusFiltro:1,  
        },
        computed:{
         Registered:function() {
          this.rows=0;
          return this.users.sort((a,b) => {
            let modifier = 1;
            if(this.currentSortDir === 'desc')
              modifier = -1;
            if(a[this.currentSort] < b[this.currentSort])
              return -1 * modifier;
            if(a[this.currentSort] > b[this.currentSort])
               return 1 * modifier;
            return 0;
           }).filter((row, index) => {
            let start = (this.currentPage-1)*this.pageSize;
            let end = this.currentPage*this.pageSize;
            if(index >= start && index < end){
               this.rows+=1;
               return true;
           }
          });
         },
        },
        methods:{
         nextPage:function() {
           if((this.currentPage*this.pageSize) < this.users.length) this.currentPage++;
         },// nextPage:function() 
         prevPage:function() {
           if(this.currentPage > 1) this.currentPage--;
         },//prevPage:function() 
         sort:function(s) {
           //if s == current sort, reverse
           if(s === this.currentSort) {
             this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
           }
             this.currentSort = s;
         },//  sort:function(s)  
         filter:function(){
            let searched=[];
            if(this.search){
               for(let i in this.users){
                  if(this.users[i].name.toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.users[i].email.toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.users[i].rol.toLowerCase().trim().search(this.search.toLowerCase())!=-1){
                     this.statusFiltro=1;
                     searched.push(this.users[i]);
                  }//if(this.users[i].name.toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.users[i].email.toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.users[i].rol.toLowerCase().trim().search(this.search.toLowerCase())!=-1)
               }//for(let i in this.users)
               if(searched.length)
                  this.users=searched;
               else{
                  this.statusFiltro=0;
                  this.users={!! $users ? $users : "''"!!};
               }// if(searched.length)
            }else{
             this.statusFiltro=1;
             this.users={!! $users ? $users : "''"!!};
            }//if(this.search)
         },// filtrar:function()
         validEmail: function (email) {
           var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
           return re.test(email);
         },//validEmail
         clear:function(){
            this.data={
               name:'',
               email:'',
               password:'',
               passwordConfirmation:'',
               rol:0,
            };  
            this.nameRequired=false;
            this.emailRequired=false;
            this.passwordRequired=false;
            this.passwordConfirmationRequired=false;
            this.rolRequired=false;
            search='';
         },//clear:function() 
         register:function(){
            let self = this;
            var name=this.data.name;
            var email=this.data.email;
            var password=this.data.password;
            var passwordConfirmation=this.data.passwordConfirmation;
            var rol=this.data.rol;
            this.nameRequired=false;
            this.emailRequired=false;
            this.passwordRequired=false;
            this.passwordConfirmationRequired=false;
            this.rolRequired=false;
            if(name==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo nombre es un campo obligatorio!');
               this.nameRequired=true;
            }//if(name=="")
            else if(name.length<3){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo nombre acepta un minimo de 3 caracteres');
               this.nameRequired=true;
            }//else if(name.length<3)
            else if(email==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo correo electrónico es un campo obligatorio!');
               this.emailRequired=true;
            }//else if(email=="")
            else if(!this.validEmail(email)){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El correo electrónico  introducido es invalido!');
               this.emailRequired=true;
            }//else if(!this.validEmail(email))
            else if(rol==0){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo rol es un campo obligatorio!');
               this.rolRequired=true;
            }//if(name=="")
            else if(password==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo contraseña es un campo obligatorio!');
               this.passwordRequired=true;
            }//else if(password=="")
            else if(password.length<6){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo contraseña acepta un minimo de 6 caracteres!');
               this.passwordRequired=true;
            }//else if(password.length<6)
            else if(passwordConfirmation==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo verificación de la contraseña es un campo obligatorio!');
               this.passwordConfirmationRequired=true;
            }//else if(passwordConfirmation=="")
            else if(passwordConfirmation.length<6){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo verificación de la contraseña acepta un minimo de 6 caracteres!');
               this.passwordConfirmationRequired=true;
            }//else if(passwordConfirmation.length<6)
            else if(password!=passwordConfirmation){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo verificación de la contraseña acepta un minimo de 6 caracteres!');
               this.passwordRequired=true;
               this.passwordConfirmationRequired=true;
            }//else if(passwordConfirmation.length<6)
            else{
               axios.post('{{ url("AddUsers") }}', {
                  name:name,
                  email:email,
                  password:password,
                  passwordConfirmation:passwordConfirmation,
                  rol:rol,
               }).then(function (response) {
                  if(response.data.status=="success"){
                     self.users=response.data.users;
                     self.clear();
                     $.Notification.autoHideNotify('success', 'top right', 'Información','Registro Satisfactorio.');
                  }//if(response.data.status=="success")
                  else if(response.data.status=="error"){                     
                     $.each(response.data.mensaje, function( key, value ) {
                        $.Notification.autoHideNotify('error', 'top right', 'Información',''+value);
                     });
                  }//if(response.data.status=="error")
               }).catch(function (error) {
                  console.log(error);
               });
               /*Fin envio de datos a la base de datos*/
            }//else if(name=="")   
         },//register
         captureRecord: function(value){
            this.data.name=value.name;
            this.data.email=value.email;
            this.data.rol=value.rol;
            this.id=value.id;         
            this.nameRequired=false;
            this.emailRequired=false;
            this.passwordRequired=false;
            this.passwordConfirmationRequired=false;
            this.rolRequired=false;
            this.buttonNameUno="Cancelar";
            this.buttonNameDos="Actualizar";
            this.change=1;
         },
         cancel:function(){
            this.data={
               name:'',
               email:'',
               password:'',
               passwordConfirmation:'',
               rol:0,
            };  
            this.nameRequired=false;
            this.emailRequired=false;
            this.passwordRequired=false;
            this.passwordConfirmationRequired=false;
            this.rolRequired=false;
            this.id="";         
            this.buttonNameUno="Limpiar";
            this.buttonNameDos="Registrar"; 
            this.change=0;
         },
         update:function(){
            let self = this;
            var id=this.id;
            var name=this.data.name;
            var email=this.data.email;
            var password=this.data.password;
            var passwordConfirmation=this.data.passwordConfirmation;
            var rol=this.data.rol;
            this.nameRequired=false;
            this.emailRequired=false;
            this.passwordRequired=false;
            this.passwordConfirmationRequired=false;
            this.rolRequired=false;
            var flag=0;
            if(name==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo nombre es un campo obligatorio!');
               this.nameRequired=true;
               flag=1;
            }//if(name=="")
            else if(name.length<3){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo nombre acepta un minimo de 3 caracteres');
               this.nameRequired=true;
               flag=1;
            }//else if(name.length<3)
            else if(email==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo correo electrónico es un campo obligatorio!');
               this.emailRequired=true;
               flag=1;
            }//else if(email=="")
            else if(!this.validEmail(email)){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El correo electrónico  introducido es invalido!');
               this.emailRequired=true;
               flag=1;
            }//else if(!this.validEmail(email))
            else if(rol==0){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo rol es un campo obligatorio!');
               this.rolRequired=true;
               flag=1;
            }//if(name=="")
            else if(password!=""){
               if(password.length<6){
                  $.Notification.autoHideNotify('error', 'top right', 'Información','El campo contraseña acepta un minimo de 6 caracteres!');
                  this.passwordRequired=true;
                  flag=1;
               }//else if(password.length<6)
               else if(passwordConfirmation==""){
                  $.Notification.autoHideNotify('error', 'top right', 'Información','El campo verificación de la contraseña es un campo obligatorio!');
                  this.passwordConfirmationRequired=true;
                  flag=1;
               }//else if(passwordConfirmation=="")
               else if(passwordConfirmation.length<6){
                  $.Notification.autoHideNotify('error', 'top right', 'Información','El campo verificación de la contraseña acepta un minimo de 6 caracteres!');
                  this.passwordConfirmationRequired=true;
                  flag=1;
               }//else if(passwordConfirmation.length<6)
               else if(password!=passwordConfirmation){
                  $.Notification.autoHideNotify('error', 'top right', 'Información','El campo verificación de la contraseña acepta un minimo de 6 caracteres!');
                  this.passwordRequired=true;
                  this.passwordConfirmationRequired=true;
                  flag=1;
               }//else if(passwordConfirmation.length<6)  
            }//else if(password!="")
            if(flag==0){
               axios.post('{{ url("UpdateUsers") }}', {
                  id:id,
                  name:name,
                  email:email,
                  password:password,
                  passwordConfirmation:passwordConfirmation,
                  rol:rol,
               }).then(function (response) {
                  if(response.data.status=="success"){
                     self.users=response.data.users;
                     self.cancel();
                     $.Notification.autoHideNotify('success', 'top right', 'Información','Actualización Satisfactoria.');
                  }//if(response.data.status=="success")
                  else if(response.data.status=="error"){                     
                     $.each(response.data.mensaje, function( key, value ) {
                        $.Notification.autoHideNotify('error', 'top right', 'Información',''+value);
                     });
                  }//if(response.data.status=="error")
               }).catch(function (error) {
                  console.log(error);
               });
               /*Fin envio de datos a la base de datos*/
            }//else if(name=="")   
         },//update:function()
         destroy:function(value){
            let self = this;
            var id=value.id;  
            Swal({
                 title: 'Información',
                 text: "¿Esta seguro de eliminar este registro?",
                 type: 'warning',
                 showCancelButton: true,
                 confirmButtonColor: '#3085d6',
                 cancelButtonColor: '#d33',
                 confirmButtonText: '¡Sí, bórralo!!'
            }).then((result) => {
             if (result.value) {
               //
               axios.post('{{ url("DeleteUsers") }}', {id:id}).then(response => {
                  if(response.data.status=="success"){
                     self.users=response.data.users;
                     self.clear();
                     $.Notification.autoHideNotify('success', 'top right', 'Información','Registro eliminado con éxito.');
                  }//if(response.data.status=="success")
                  if(response.data.status=="error"){
                     self.clear();                           
                     $.each(response.data.mensaje, function( key, value ) {
                        $.Notification.autoHideNotify('error', 'top right', 'Información',''+value);
                     });
                  }//if(response.data.status=="error")
               }).catch(error => {
                  console.log(error);
               });
               //
            }
          })
         },//destroy:function()
         ActionsCru:function(){
           if(this.change==0){
             this.register();
           }//if(this.change==0)
           else if(this.change==1){
             this.update();
           }//else if(this.change==1)
         },//ActionsCru:function()
         ActionsCc:function(){
            if(this.change==0){
             this.clear();
           }//if(this.change==0)
           else if(this.change==1){
             this.cancel();
           }//else if(this.change==1)
         }//ActionsCc:function()
        },//methods
   });//const app= new Vue
</script>
@endpush


@extends('layouts.dashboard')
@section('subContenido')
<div class="row" id="register">
   <div class="col-md-12">
      <div class="card-box">
         <h4 class="m-t-0 header-title">GESTIÓN DE MONEDAS</h4>
         <p class="text-muted m-b-30 font-13">
            Los datos con<code class="highlighter-rouge"> Asterisco </code> son requeridos
         </p>
         <div class="form-row justify-content-center">
            <div class="form-group col-md-4">
               <label for="name" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Nombre</label>
               <input id="name" class="form-control" v-bind:class="{ 'is-invalid': nameRequired }" minlength="3" maxlength="20"  type="text" v-model="data.name"  pattern="[a-zA-Z]*" onkeypress="return soloLetrasNumeros(event)" placeholder="Ingrese name"required >
            </div>
            <div class="form-group col-md-2">
               <label for="abbreviation" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Abreviación</label>
               <input id="abbreviation" class="form-control" v-bind:class="{ 'is-invalid': abbreviationRequired }"  maxlength="5"  type="text" v-model="data.abbreviation" onkeypress="return soloLetras(event)"  pattern="[a-zA-Z]*" placeholder="Ingrese la abreviaciòn" required>
            </div>
            <div class="form-group col-md-2">
               <label for="symbol" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Simbolo</label>
               <input id="symbol" class="form-control" v-bind:class="{ 'is-invalid': symbolRequired }"  maxlength="5"  type="text" v-model="data.symbol"   placeholder="Ingrese el simbolo" required>            
            </div>
            <div class="form-group col-12 col-md-2">
               <label for="position" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Posición del simbolo</label>
               <select v-model="data.position" class="form-control" v-bind:class="{ 'is-invalid': positionRequired }">
                  <option value="0">Seleccione la posición</option>
                  <option value="RIGHT">Derecha</option>
                  <option value="LEFT">Izquierda</option>
               </select>
            </div>
            <div class="form-group col-12 col-md-2">
               <label for="position" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Estado</label>
               <select v-model="data.status" class="form-control" v-bind:class="{ 'is-invalid': statusRequired }">
                  <option value="0">Seleccione el estado</option>
                  <option value="ACTIVE">Activo</option>
                  <option value="INACTIVE">Inactivo</option>
               </select>
            </div>
            <div class="form-group col-md-12 text-center">
               <button type="submit" class="btn btn-warning" @click="ActionsCc">@{{buttonNameUno}}</button>
               <button type="submit" class="btn btn-primary" @click="ActionsCru">@{{buttonNameDos}}</button>
            </div>
            <div class="col-12 col-md-12 col-lg-12">
               <div id="tabla" v-show='coins.length'>
                  <div class="row">
                  <div class="form-group col-md-12 text-center">
                      <h4 class="text-dark  header-title m-t-0 m-b-20 pt-5">LISTADO DE MONEDAS</h4>
                  </div>
                     <div class="col-12 col-md-9 col-lg-9">
                        <br>
                        <div class="mt-2 form-inline font-weight-bold">
                           Mostrar
                           <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                              <option v-for="option in optionspageSize" v-bind:value="option.value">
                                 @{{ option.text }}
                              </option>
                           </select>
                           registros
                        </div>
                     </div>
                     <div class="col-12 col-md-3 col-lg-3">
                        <div class="form-group">
                           <label for="Buscar" class="font-weight-bold">Buscar:</label>
                           <input id="search" class="form-control form-control-sm"  maxlength="200"  type="text" v-model="search" v-on:keyup="filter" required>
                        </div>
                     </div>
                  </div>
                  <table  class="table table-bordered">
                     <thead class="bg-primary text-white">
                        <tr>
                           <th scope="col" style="cursor:pointer; width:18%;" class="text-center" @click="sort('name')">Nombre</th>
                           <th scope="col" style="cursor:pointer; width:18%;" class="text-center" @click="sort('abbreviation')">Abreviación</th>
                           <th scope="col" style="cursor:pointer; width:18%;" class="text-center" @click="sort('symbol')">Simbolo</th>
                           <th scope="col" style="cursor:pointer; width:18%;" class="text-center" @click="sort('position')">Posición</th>
                           <th scope="col" style="cursor:pointer; width:18%;" class="text-center" @click="sort('status')">Estado</th>
                           <th scope="col" class="text-center width:10%;">Acciones</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr v-for="(value,index) in Registered" v-show='statusFiltro'>
                           <td class="text-justify">
                              <p class="anchoparrafo">@{{value.name}}</p>
                           </td>
                           <td class="text-justify">
                              <p class="anchoparrafo">@{{value.abbreviation}}</p>
                           </td>
                           <td class="text-justify">
                              <p class="anchoparrafo">@{{value.symbol}}</p>
                           </td>
                           <td class="text-justify">
                              <p class="anchoparrafo">@{{value.positionName}}</p>
                           </td>
                           <td class="text-justify">
                              <p class="anchoparrafo">@{{value.statusName}}</p>
                           </td>
                           <td class="text-center align-middle">
                              <button class="btn btn-icon waves-effect btn-info m-b-5"  @click="captureRecord(value)" title="Editar"> <i class="fa fa-edit"></i> </button>
                              <button class="btn btn-icon waves-effect btn-danger m-b-5" title="Borrar"  @click="destroy(value)"> <i class="fa fa-trash-o"></i> </button>
                           </td>
                        </tr>
                        <tr v-show='!statusFiltro'>
                           <td class="text-justify font-weight-bold" colspan="3">No se encontraron registros</td>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <div class="">
                     <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{coins.length}}</strong>
                     <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de: 0 </strong>
                     <div style="float:right">
                        <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                        <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                        <div class="row ml-2">
                           <strong>Página:  @{{currentPage}}</strong>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         </form>
      </div>
   </div>
</div>
@endsection
@push('scripts')
<script>
   const app= new Vue({
        el:'#register',
        data:{
              data:{
                  name:'',
                  abbreviation:'',
                  symbol:'',
                  position:0,
                  status:0,
              },
              id:'',
              nameRequired:false,
              abbreviationRequired:false,
              symbolRequired:false,
              positionRequired:false,
              statusRequired:false,
              coins:{!! $coins ? $coins : "''"!!},
              buttonNameUno:"Limpiar",
              buttonNameDos:"Registrar",
              change:0,
              search:'',
              currentSort:'id',//campo por defecto que tomara para ordenar
              currentSortDir:'desc',//order asc
              pageSize:'5',//Registros por pagina
              optionspageSize: [
               { text: '5', value: 5 },
               { text: '10', value: 10 },
               { text: '25', value: 25 },
               { text: '50', value: 50 },
               { text: '100', value: 100 }
              ],//Registros por pagina
              currentPage:1,//Pagina 1
              statusFiltro:1,  
        },
        computed:{
         Registered:function() {
          this.rows=0;
          return this.coins.sort((a,b) => {
            let modifier = 1;
            if(this.currentSortDir === 'desc')
              modifier = -1;
            if(a[this.currentSort] < b[this.currentSort])
              return -1 * modifier;
            if(a[this.currentSort] > b[this.currentSort])
               return 1 * modifier;
            return 0;
           }).filter((row, index) => {
            let start = (this.currentPage-1)*this.pageSize;
            let end = this.currentPage*this.pageSize;
            if(index >= start && index < end){
               this.rows+=1;
               return true;
           }
          });
         },
        },
        methods:{
         nextPage:function() {
           if((this.currentPage*this.pageSize) < this.coins.length) this.currentPage++;
         },// nextPage:function() 
         prevPage:function() {
           if(this.currentPage > 1) this.currentPage--;
         },//prevPage:function() 
         sort:function(s) {
           //if s == current sort, reverse
           if(s === this.currentSort) {
             this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
           }
             this.currentSort = s;
         },//  sort:function(s)  
         filter:function(){
            let searched=[];
            if(this.search){
               for(let i in this.coins){
                  if(this.coins[i].name.toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.coins[i].abbreviation.toLowerCase().trim().search(this.search.toLowerCase())!=-1){
                     this.statusFiltro=1;
                     searched.push(this.coins[i]);
                  }// if(this.coins[i].name.toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.coins[i].abbreviation.toLowerCase().trim().search(this.search.toLowerCase())!=-1)
               }//for(let i in this.coins)
               if(searched.length)
                  this.coins=searched;
               else{
                  this.statusFiltro=0;
                  this.coins={!! $coins ? $coins : "''"!!};
               }// if(searched.length)
            }else{
             this.statusFiltro=1;
             this.coins={!! $coins ? $coins : "''"!!};
            }//if(this.search)
         },// filtrar:function()
         clear:function(){
            this.data={
               name:'',
               abbreviation:'',
               symbol:'',
               position:0,
               status:0,
            };  
            this.nameRequired=false;
            this.abbreviationRequired=false;
            this.symbolRequired=false;
            this.positionRequired=false;
            this.statusRequired=false;
            search='';
         },//clear:function() 
         register:function(){
            let self = this;
            var name=this.data.name;
            var abbreviation=this.data.abbreviation;
            var symbol=this.data.symbol;
            var position=this.data.position;
            var status=this.data.status;
            this.nameRequired=false;
            this.abbreviationRequired=false;
            this.symbolRequired=false;
            this.positionRequired=false;
            this.statusRequired=false;
            if(name==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo nombre es un campo obligatorio!');
               this.nameRequired=true;
            }//if(name=="")
            else if(name.length<3){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo nombre acepta un minimo de 3 caracteres');
               this.nameRequired=true;
            }//else if(name.length<3)
            else if(abbreviation==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo abreviación es un campo obligatorio!');
               this.abbreviationRequired=true;
            }//if(abbreviation=="")
            else if(abbreviation.length<2){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo abreviación acepta un minimo de 2 caracteres');
               this.abbreviationRequired=true;
            }//else if(abbreviation.length<2)
            else if(symbol==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo simbolo es un campo obligatorio!');
               this.symbolRequired=true;
            }//if(symbol=="")
            else if(position==0){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo posición del simbolo es un campo obligatorio!');
               this.positionRequired=true;
            }//if(position==0)
            else if(status==0){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo estado es un campo obligatorio!');
               this.statusRequired=true;
            }//if(status==0)
            else{
               axios.post('{{ url("AddCoins") }}', {
                  name:name,
                  abbreviation:abbreviation,
                  symbol:symbol,
                  position:position,
                  status:status,
               }).then(function (response) {
                  if(response.data.status=="success"){
                     self.coins=response.data.coins;
                     self.clear();
                     $.Notification.autoHideNotify('success', 'top right', 'Información','Registro Satisfactorio.');
                  }//if(response.data.status=="success")
                  else if(response.data.status=="error"){                     
                     $.each(response.data.mensaje, function( key, value ) {
                        $.Notification.autoHideNotify('error', 'top right', 'Información',''+value);
                     });
                  }//if(response.data.status=="error")
               }).catch(function (error) {
                  console.log(error);
               });
               /*Fin envio de datos a la base de datos*/
            }//else if(name=="")   
         },//register
         captureRecord: function(value){
            this.data.name=value.name;
            this.data.abbreviation=value.abbreviation;
            this.data.symbol=value.symbol;
            this.data.position=value.position;
            this.data.status=value.status;
            this.id=value.id;         
            this.nameRequired=false;
            this.abbreviationRequired=false;
            this.symbolRequired=false;
            this.positionRequired=false;
            this.statusRequired=false;
            this.buttonNameUno="Cancelar";
            this.buttonNameDos="Actualizar";
            this.change=1;
         },
         cancel:function(){
            this.data={
               name:'',
               abbreviation:'',
               symbol:'',
               position:0,
               status:0,
            };  
            this.nameRequired=false;
            this.abbreviationRequired=false;
            this.symbolRequired=false;
            this.positionRequired=false;
            this.statusRequired=false;
            this.id="";         
            this.buttonNameUno="Limpiar";
            this.buttonNameDos="Registrar"; 
            this.change=0;
         },
         update:function(){
            let self = this;
            var id=this.id;
            var name=this.data.name;
            var abbreviation=this.data.abbreviation;
            var symbol=this.data.symbol;
            var position=this.data.position;
            var status=this.data.status;
            this.nameRequired=false;
            this.abbreviationRequired=false;
            this.symbolRequired=false;
            this.positionRequired=false;
            this.statusRequired=false;
            if(name==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo nombre es un campo obligatorio!');
               this.nameRequired=true;
            }//if(name=="")
            else if(name.length<3){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo nombre acepta un minimo de 3 caracteres');
               this.nameRequired=true;
            }//else if(name.length<3)
            else if(abbreviation==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo abreviación es un campo obligatorio!');
               this.abbreviationRequired=true;
            }//if(abbreviation=="")
            else if(abbreviation.length<2){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo abreviación acepta un minimo de 2 caracteres');
               this.abbreviationRequired=true;
            }//else if(abbreviation.length<2)
            else if(symbol==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo simbolo es un campo obligatorio!');
               this.symbolRequired=true;
            }//if(symbol=="")
            else if(position==0){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo posición del simbolo es un campo obligatorio!');
               this.positionRequired=true;
            }//if(position==0)
            else if(status==0){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo estado es un campo obligatorio!');
               this.statusRequired=true;
            }//if(status==0)
            else{
               axios.post('{{ url("UpdateCoins") }}', {
                  id:id,
                  name:name,
                  abbreviation:abbreviation,
                  symbol:symbol,
                  position:position,
                  status:status,
               }).then(function (response) {
                  if(response.data.status=="success"){
                     self.coins=response.data.coins;
                     self.cancel();
                     $.Notification.autoHideNotify('success', 'top right', 'Información','Actualización Satisfactoria.');
                  }//if(response.data.status=="success")
                  else if(response.data.status=="error"){                     
                     $.each(response.data.mensaje, function( key, value ) {
                        $.Notification.autoHideNotify('error', 'top right', 'Información',''+value);
                     });
                  }//if(response.data.status=="error")
               }).catch(function (error) {
                  console.log(error);
               });
               /*Fin envio de datos a la base de datos*/
            }//else
         },//update:function()
         destroy:function(value){
            let self = this;
            var id=value.id;  
            Swal({
                 title: 'Información',
                 text: "¿Esta seguro de eliminar este registro?",
                 type: 'warning',
                 showCancelButton: true,
                 confirmButtonColor: '#3085d6',
                 cancelButtonColor: '#d33',
                 confirmButtonText: '¡Sí, bórralo!!'
            }).then((result) => {
             if (result.value) {
               //
               axios.post('{{ url("DeleteCoins") }}', {id:id}).then(response => {
                  if(response.data.status=="success"){
                     self.coins=response.data.coins;
                     self.clear();
                     $.Notification.autoHideNotify('success', 'top right', 'Información','Registro eliminado con éxito.');
                  }//if(response.data.status=="success")
                  if(response.data.status=="error"){
                     self.clear();                           
                     $.each(response.data.mensaje, function( key, value ) {
                        $.Notification.autoHideNotify('error', 'top right', 'Información',''+value);
                     });
                  }//if(response.data.status=="error")
               }).catch(error => {
                  console.log(error);
               });
               //
            }
          })
         },//destroy:function()
         ActionsCru:function(){
           if(this.change==0){
             this.register();
           }//if(this.change==0)
           else if(this.change==1){
             this.update();
           }//else if(this.change==1)
         },//ActionsCru:function()
         ActionsCc:function(){
            if(this.change==0){
             this.clear();
           }//if(this.change==0)
           else if(this.change==1){
             this.cancel();
           }//else if(this.change==1)
         }//ActionsCc:function()
        },//methods
   });//const app= new Vue
</script>
@endpush


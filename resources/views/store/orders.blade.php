@extends('layouts.dashboard')
@section('subContenido')
<div class="row" id="register">
   <div class="col-md-12">
      <div class="card-box">
         
         <div class="form-row justify-content-center">        
            <div class="col-12 col-md-12 col-lg-12">
               <div id="tabla" v-show='orders.length'>
                  <div class="row">
                  <div class="form-group col-md-12 text-center">
                      <h4 class="text-dark  header-title m-t-0 m-b-20 pt-5">LISTADO DE ORDENES</h4>
                  </div>
                     <div class="col-12 col-md-9 col-lg-9">
                        <br>
                        <div class="mt-2 form-inline font-weight-bold">
                           Mostrar
                           <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                              <option v-for="option in optionspageSize" v-bind:value="option.value">
                                 @{{ option.text }}
                              </option>
                           </select>
                           registros
                        </div>
                     </div>
                     <div class="col-12 col-md-3 col-lg-3">
                        <div class="form-group">
                           <label for="Buscar" class="font-weight-bold">Buscar:</label>
                           <input id="search" class="form-control form-control-sm"  maxlength="200"  type="text" v-model="search" v-on:keyup="filter" required>
                        </div>
                     </div>
                  </div>
                  <table  class="table table-bordered">
                     <thead class="bg-primary text-white">
                        <tr>
                           <th scope="col" style="cursor:pointer; width:18%;" class="text-center" @click="sort('created_at')">Fecha</th>
                           <th scope="col" style="cursor:pointer; width:18%;" class="text-center" @click="sort('nameUser')">Usuario</th>
                           <th scope="col" style="cursor:pointer; width:18%;" class="text-center" @click="sort('subtotal')">SubTotal</th>
                           <th scope="col" style="cursor:pointer; width:18%;" class="text-center" @click="sort('shipping')">Envio</th>
                           <th scope="col" style="cursor:pointer; width:18%;" class="text-center" @click="sort('total')">Total</th>
                           <th scope="col" class="text-center width:10%;">Acciones</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr v-for="(value,index) in Registered" v-show='statusFiltro'>
                           <td class="text-justify">
                              <p class="anchoparrafo">@{{value.created_at}}</p>
                           </td>
                           <td class="text-justify">
                              <p class="anchoparrafo">@{{value.name}}</p>
                           </td>
                           <td class="text-justify">
                              <p class="anchoparrafo">@{{value.subtotal}}</p>
                           </td>
                           <td class="text-justify">
                              <p class="anchoparrafo">@{{value.nameUser}}</p>
                           </td>
                           <td class="text-justify">
                              <p class="anchoparrafo">@{{value.total}}</p>
                           </td>
                           <td class="text-center align-middle">
                              <button class="btn btn-icon waves-effect btn-info m-b-5"  @click="captureRecord(value)" title="Editar"> <i class="fa fa-edit"></i> </button>
                              <button class="btn btn-icon waves-effect btn-danger m-b-5" title="Borrar"  @click="destroy(value)"> <i class="fa fa-trash-o"></i> </button>
                           </td>
                        </tr>
                        <tr v-show='!statusFiltro'>
                           <td class="text-justify font-weight-bold" colspan="3">No se encontraron registros</td>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <div class="">
                     <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{orders.length}}</strong>
                     <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de: 0 </strong>
                     <div style="float:right">
                        <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                        <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                        <div class="row ml-2">
                           <strong>Página:  @{{currentPage}}</strong>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         </form>
      </div>
   </div>
</div>
@endsection
@push('scripts')
<script>
   const app= new Vue({
        el:'#register',
        data:{
              id:'',  
              orders:{!! $orders ? $orders : "''"!!},
              search:'',
              currentSort:'id',//campo por defecto que tomara para ordenar
              currentSortDir:'desc',//order asc
              pageSize:'5',//Registros por pagina
              optionspageSize: [
               { text: '5', value: 5 },
               { text: '10', value: 10 },
               { text: '25', value: 25 },
               { text: '50', value: 50 },
               { text: '100', value: 100 }
              ],//Registros por pagina
              currentPage:1,//Pagina 1
              statusFiltro:1,  
        },
        computed:{
         Registered:function() {
          this.rows=0;
          return this.orders.sort((a,b) => {
            let modifier = 1;
            if(this.currentSortDir === 'desc')
              modifier = -1;
            if(a[this.currentSort] < b[this.currentSort])
              return -1 * modifier;
            if(a[this.currentSort] > b[this.currentSort])
               return 1 * modifier;
            return 0;
           }).filter((row, index) => {
            let start = (this.currentPage-1)*this.pageSize;
            let end = this.currentPage*this.pageSize;
            if(index >= start && index < end){
               this.rows+=1;
               return true;
           }
          });
         },
        },
        methods:{
         nextPage:function() {
           if((this.currentPage*this.pageSize) < this.orders.length) this.currentPage++;
         },// nextPage:function() 
         prevPage:function() {
           if(this.currentPage > 1) this.currentPage--;
         },//prevPage:function() 
         sort:function(s) {
           //if s == current sort, reverse
           if(s === this.currentSort) {
             this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
           }
             this.currentSort = s;
         },//  sort:function(s)  
         filter:function(){
            let searched=[];
            if(this.search){
               for(let i in this.orders){
                  if(this.orders[i].invoice_number.toLowerCase().trim().search(this.search.toLowerCase())!=-1){
                     this.statusFiltro=1;
                     searched.push(this.orders[i]);
                  }//if(this.orders[i].invoice_number.toLowerCase().trim().search(this.search.toLowerCase())!=-1)
               }//for(let i in this.orders)
               if(searched.length)
                  this.orders=searched;
               else{
                  this.statusFiltro=0;
                  this.orders={!! $orders ? $orders : "''"!!};
               }// if(searched.length)
            }else{
             this.statusFiltro=1;
             this.orders={!! $orders ? $orders : "''"!!};
            }//if(this.search)
         },// filtrar:function()
         captureRecord: function(value){
            this.id=value.id;         
         },
         destroy:function(value){
            let self = this;
            var id=value.id;  
            Swal({
                 title: 'Información',
                 text: "¿Esta seguro de eliminar este registro?",
                 type: 'warning',
                 showCancelButton: true,
                 confirmButtonColor: '#3085d6',
                 cancelButtonColor: '#d33',
                 confirmButtonText: '¡Sí, bórralo!!'
            }).then((result) => {
             if (result.value) {
               //
               axios.post('{{ url("DeleteOrders") }}', {id:id}).then(response => {
                  if(response.data.status=="success"){
                     self.orders=response.data.orders;
                     self.clear();
                     $.Notification.autoHideNotify('success', 'top right', 'Información','Registro eliminado con éxito.');
                  }//if(response.data.status=="success")
                  if(response.data.status=="error"){
                     self.clear();                           
                     $.each(response.data.mensaje, function( key, value ) {
                        $.Notification.autoHideNotify('error', 'top right', 'Información',''+value);
                     });
                  }//if(response.data.status=="error")
               }).catch(error => {
                  console.log(error);
               });
               //
            }
          })
         },//destroy:function()
        },//methods
   });//const app= new Vue
</script>
@endpush


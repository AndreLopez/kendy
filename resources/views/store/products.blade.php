@extends('layouts.dashboard')
@section('subContenido')
<div class="row" id="register">
   <div class="col-md-12">
      <div class="card-box">
         <h4 class="m-t-0 header-title">GESTIÓN DE PRODUCTOS</h4>
         <p class="text-muted m-b-30 font-13">
            Los datos con<code class="highlighter-rouge"> Asterisco </code> son requeridos
         </p>
         <div class="form-row">
           <div class="form-group col-md-4">
               <label for="sku" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Código del Producto</label>
               <input type="text" class="form-control text-uppercase" id="sku" v-bind:class="{ 'is-invalid': skuRequired }" placeholder="Código del Producto" maxlength="30" onkeypress="return sku(event)"  v-model.trim="data.sku" required>
           </div>
           <div class="form-group col-12 col-md-4">
               <label for="product_category_id" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Categoría</label>
               <select v-model="data.product_category_id" class="form-control" @change="findSubCategories" v-bind:class="{ 'is-invalid': product_category_idRequired }">
                  <option value="0">Seleccione la categoria</option>
                  <option v-for="option in categoriesFatherProduct" v-bind:value="option.id">
                        @{{ option.name }}
                  </option>
               </select>
            </div>
            <div class="form-group col-12 col-md-4">
               <label for="product_category_parent_id" class="col-form-label">SubCategoría</label>
               <select v-model="data.product_category_parent_id" class="form-control">
                  <option value="0">Seleccione la subcategoria</option>
                  <option v-for="option in subCategories" v-bind:value="option.id">
                        @{{ option.name }}
                  </option>
               </select>
            </div>
            <div class="form-group col-md-4">
               <label for="name" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Nombre</label>
               <input type="text" class="form-control" id="name" v-bind:class="{ 'is-invalid': nameRequired }" placeholder="Nombre" maxlength="200" required>
            </div>
            <div class="form-group col-md-4">
               <label for="slug" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Url amigable</label>
               <input type="text" class="form-control" id="slug" v-bind:class="{ 'is-invalid': slugRequired }" placeholder="Url amigable" readonly required> 
            </div>
            <div class="form-group col-md-4">
               <label for="color_id" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Color del Producto</label>
               <select v-model="data.color_id" class="form-control">
                  <option value="0">Seleccione un color</option>
                  <option v-for="option in colors" v-bind:value="option.id">
                        @{{ option.name }}
                  </option>
               </select>
            </div>
            <div class="form-group col-md-12">
               <label for="description" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Descripción</label>
               <textarea type="text" class="form-control" id="description" placeholder="Descripción" v-bind:class="{ 'is-invalid': descriptionRequired }" v-model.trim="data.description"> </textarea>
            </div>
            <div class="form-group col-md-2">
               <label for="price" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Precio del Producto (@{{coin.abbreviation}})</label>
               <input type="number" class="form-control" id="price" v-bind:class="{ 'is-invalid': priceRequired }" placeholder="Precio del Producto" maxlength="12"  v-model.number="data.price" required>
            </div>
            <div class="form-group col-12 col-md-2 col-lg-2">
               <label for="visible" class="col-form-label"><i class="fa fa-asterisk text-danger"></i>Visible</label>
               <br>
               <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="visible1" name="visible" class="custom-control-input" value="true" v-model="data.visible" v-bind:disabled="change==2">
                  <label class="custom-control-label" for="visible1">Visible</label>
               </div>
               <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="visible2" name="visible" class="custom-control-input" value="false" v-model="data.visible" v-bind:disabled="change==2">
                  <label class="custom-control-label" for="visible2">No visible</label>
               </div>
            </div>
            <div class="form-group col-12 col-md-8 col-lg-8">
               <label for="image" class="col-form-label">Imagen</label><br>
               <div class="fileUpload btn btn-primary mt-2 mb-2 col-1">
                     <i class="fa fa-upload"></i>
                     <span></span>
                     <input type="file" class="upload" id="logo"  accept="image/*" v-on:change="fileImage" v-bind:disabled="change==2"/>
                  </div>
                  <strong v-if="data.image_name!=null">@{{data.image_name}}</strong>
                  <strong v-else>Selecione una imagen</strong>
            </div>
            <div class="form-group col-md-12 text-center">
               <button type="submit" class="btn btn-warning" @click="ActionsCc">@{{buttonNameUno}}</button>
               <button type="submit" class="btn btn-primary" @click="ActionsCru">@{{buttonNameDos}}</button>
            </div>
         
            <div class="col-12 col-md-12 col-lg-12">
               <div id="tabla" v-show='products.length'>
                  <div class="row">
                  <div class="form-group col-md-12 text-center">
                      <h4 class="text-dark  header-title m-t-0 m-b-20 pt-5">LISTADO DE PRODUCTOS</h4>
                  </div>
                     <div class="col-12 col-md-9 col-lg-9">
                        <br>
                        <div class="mt-2 form-inline font-weight-bold">
                           Mostrar
                           <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                              <option v-for="option in optionspageSize" v-bind:value="option.value">
                                 @{{ option.text }}
                              </option>
                           </select>
                           registros
                        </div>
                     </div>
                     <div class="col-12 col-md-3 col-lg-3">
                        <div class="form-group">
                           <label for="Buscar" class="font-weight-bold">Buscar:</label>
                           <input id="search" class="form-control form-control-sm"  maxlength="200"  type="text" v-model="search" v-on:keyup="filter" required>
                        </div>
                     </div>
                  </div>
                  <table  class="table table-bordered">
                     <thead class="bg-primary text-white">
                        <tr>
                           <th scope="col" style="cursor:pointer; width:10%;" class="text-center" @click="sort('sku')">Código</th>
                           <th scope="col" style="cursor:pointer; width:15%;" class="text-center" @click="sort('file')">Imagen</th>
                           <th scope="col" style="cursor:pointer; width:15%;" class="text-center" @click="sort('name')">Nombre</th>
                           <th scope="col" style="cursor:pointer; width:15%;" class="text-center" @click="sort('categoryName')">Categoría</th>
                           <th scope="col" style="cursor:pointer; width:15%;" class="text-center" @click="sort('subCategoryName')">SubCategoría</th>
                           <th scope="col" style="cursor:pointer; width:10%;" class="text-center" @click="sort('price')">Precio (@{{coin.abbreviation}})</th>
                           <th scope="col" style="cursor:pointer; width:10%;" class="text-center" @click="sort('visible')">Estado</th>
                           <th scope="col" class="text-center width:10%;">Acciones</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr v-for="(value,index) in Registered" v-show='statusFiltro'>
                           <td class="text-justify align-middle">
                              <p class="anchoparrafo">@{{value.sku}}</p>
                           </td>
                           <td class="text-justify align-middle">
                              <p class="anchoparrafo" v-if="value.file!=null"><img  v-bind:src="value.file" class="img-thumbnail sizeImg mt-2" alt="value.name"></p>
                              <p v-else><img  src="{{ asset('images/fotos/no-image.jpg') }}" class="img-thumbnail sizeImg mt-2" alt="Sin imagen"></p>
                           </td>
                           <td class="text-justify align-middle">
                              <p class="anchoparrafo">@{{value.name}}</p>
                           </td>
                           <td class="text-justify align-middle">
                              <p class="anchoparrafo">@{{value.categoryName}}</p>
                           </td>
                           <td class="text-justify align-middle">
                              <p class="anchoparrafo">@{{value.subCategoryName}}</p>
                           </td>
                           <td class="text-center align-middle">
                              <p class="anchoparrafo">@{{value.price}}</p>
                           </td>
                           <td class="text-center align-middle">
                              <p class="anchoparrafo" v-if="value.visible==true">Visible</p>
                              <p class="anchoparrafo" v-else>No Visible</p>
                           </td>
                           <td class="text-center align-middle">
                              <button class="btn btn-icon waves-effect btn-info m-b-5"  @click="captureRecord(value)" title="Editar"> <i class="fa fa-edit"></i> </button>
                              <button class="btn btn-icon waves-effect btn-danger m-b-5" title="Borrar"  @click="destroy(value)"> <i class="fa fa-trash-o"></i> </button>
                           </td>
                        </tr>
                        <tr v-show='!statusFiltro'>
                           <td class="text-justify font-weight-bold" colspan="3">No se encontraron registros</td>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <div class="">
                     <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{products.length}}</strong>
                     <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de: 0 </strong>
                     <div style="float:right">
                        <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                        <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                        <div class="row ml-2">
                           <strong>Página:  @{{currentPage}}</strong>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         </form>
      </div>
   </div>
</div>
@endsection
@push('scripts')
<script>
   const app= new Vue({
        el:'#register',
        data:{
              data:{
                  product_category_id:0,
                  product_category_parent_id:0,
                  sku:'',
                  color_id:0,
                  name:'',
                  slug:'',
                  description:'',
                  price:'',
                  visible:true,
                  file:'',
                  image_name:'Selecione una imagen',
              },
              id:'',  
              product_category_idRequired:false,
              skuRequired:false,
              nameRequired:false,
              slugRequired:false,
              color_idRequired:false,
              priceRequired:false,
              descriptionRequired:false,
              categoriesFatherProduct:{!! $categoriesFatherProduct ? $categoriesFatherProduct : "''"!!},
              coin:{!! $coin ? $coin : "''"!!},
              subCategories:"",
              colors:{!! $colors ? $colors : "''"!!},   
              products:{!! $products ? $products : "''"!!},              
              buttonNameUno:"Limpiar",
              buttonNameDos:"Registrar",
              change:0,
              search:'',
              currentSort:'id',//campo por defecto que tomara para ordenar
              currentSortDir:'desc',//order asc
              pageSize:'5',//Registros por pagina
              optionspageSize: [
               { text: '5', value: 5 },
               { text: '10', value: 10 },
               { text: '25', value: 25 },
               { text: '50', value: 50 },
               { text: '100', value: 100 }
              ],//Registros por pagina
              currentPage:1,//Pagina 1
              statusFiltro:1,  
        },
        computed:{   
         Registered:function() {
          this.rows=0;
          return this.products.sort((a,b) => {
            let modifier = 1;
            if(this.currentSortDir === 'desc')
              modifier = -1;
            if(a[this.currentSort] < b[this.currentSort])
              return -1 * modifier;
            if(a[this.currentSort] > b[this.currentSort])
               return 1 * modifier;
            return 0;
           }).filter((row, index) => {
            let start = (this.currentPage-1)*this.pageSize;
            let end = this.currentPage*this.pageSize;
            if(index >= start && index < end){
               this.rows+=1;
               return true;
           }
          });
         },
        },
        methods:{
         fileImage:function(event){
            // Reference to the DOM input element
            var input = event.target;
            let self = this;
            // Ensure that you have a file before attempting to read it
            if (input.files && input.files[0]) {
               // create a new FileReader to read this image and convert to base64 format
               var reader = new FileReader();
               // Define a callback function to run, when FileReader finishes its job
               reader.onload = (e) => {
                  // Note: arrow function used here, so that "this.imgLogo" refers to the imgLogo of Vue component
                  // Read image as base64 and set to imgLogo
                  self.data.file = e.target.result;
               }
               // Start the reader job - read file as a data url (base64 format)
               self.data.image_name=input.files[0].name;
               reader.readAsDataURL(input.files[0]);
            }
         },//file:function(event)    
         findSubCategories:function(){
            let self = this;
           if(this.data.product_category_id==0){
            this.data.product_category_parent_id=0;
            this.subCategories="";
           }//if(product_category_id==0)
           else{
            axios.post('{{ url("FindSubCategories") }}', {
                  id:this.data.product_category_id,
               }).then(function (response) {
                  if(response.data.status=="success"){
                     self.subCategories=response.data.subCategories;
                  }//if(response.data.status=="success")
                  else if(response.data.status=="error"){                     
                     $.each(response.data.mensaje, function( key, value ) {
                        $.Notification.autoHideNotify('error', 'top right', 'Información',''+value);
                     });
                  }//if(response.data.status=="error")
               }).catch(function (error) {
                  console.log(error);
               }); 
           }//else if(product_category_id==0)
         },//findSubCategories:function()
         nextPage:function() {
           if((this.currentPage*this.pageSize) < this.products.length) this.currentPage++;
         },// nextPage:function() 
         prevPage:function() {
           if(this.currentPage > 1) this.currentPage--;
         },//prevPage:function() 
         sort:function(s) {
           //if s == current sort, reverse
           if(s === this.currentSort) {
             this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
           }
             this.currentSort = s;
         },//  sort:function(s)  
         filter:function(){
            let searched=[];
            if(this.search){
               for(let i in this.products){
                  if(this.products[i].name.toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.products[i].description.toLowerCase().trim().search(this.search.toLowerCase())!=-1){
                     this.statusFiltro=1;
                     searched.push(this.products[i]);
                  }//if(this.products[i].name.toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.products[i].description.toLowerCase().trim().search(this.search.toLowerCase())!=-1)
               }//for(let i in this.products)
               if(searched.length)
                  this.products=searched;
               else{
                  this.statusFiltro=0;
                  this.products={!! $products ? $products : "''"!!};
               }// if(searched.length)
            }else{
             this.statusFiltro=1;
             this.products={!! $products ? $products : "''"!!};
            }//if(this.search)
         },// filtrar:function()
         clear:function(){
            $("#name").val("");
            $("#slug").val("");
            this.data={
               product_category_id:0,
               product_category_parent_id:0,
               sku:'',
               color_id:0,
               name:'',
               slug:'',
               description:'',
               price:'',
               visible:true,
               file:'',
               image_name:'Selecione una imagen',
            };  
            this.product_category_idRequired=false;
            this.skuRequired=false;
            this.nameRequired=false;
            this.slugRequired=false;
            this.color_idRequired=false;
            this.priceRequired=false;
            this.descriptionRequired=false;
            search='';
         },//clear:function() 
         register:function(){
            let self = this;
            var sku=this.data.sku.toUpperCase();
            var product_category_id=this.data.product_category_id;
            var product_category_parent_id=this.data.product_category_parent_id;            
            var name=$("#name").val();
            var slug=$("#slug").val();
            var color_id=this.data.color_id;
            var description=this.data.description;
            var price=this.data.price;
            var visible=this.data.visible;
            var file=this.data.file;
            var image_name=this.data.image_name;
            if(image_name=='Selecione una imagen'){
               image_name="";
            }//if(image_name=='Selecione una imagen')
            this.product_category_idRequired=false;
            this.skuRequired=false;
            this.nameRequired=false;
            this.slugRequired=false;
            this.color_idRequired=false;
            this.priceRequired=false;
            this.descriptionRequired=false;
            if(sku==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo código del producto es un campo obligatorio!');
               this.skuRequired=true;
            }//if(sku=="")
            else if(product_category_id==0){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo categoría es un campo obligatorio!');
               this.product_category_idRequired=true;
            }//else if(product_category_id==0)
            else if(name==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo nombre es un campo obligatorio!');
               this.nameRequired=true;
               this.slugRequired=true;
            }//else if(name=="")
            else if(color_id==0){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo color es un campo obligatorio!');
               this.color_idRequired=true;
            }//else if(color_id==0)
            else if(description==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo descripción es un campo obligatorio!');
               this.descriptionRequired=true;
            }//else if(description=="")
            else if(price==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo precio es un campo obligatorio!');
               this.priceRequired=true;
            }//else if(price=="")
            else if(price<=0){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo precio debe mayor a cero!');
               this.priceRequired=true;
            }//else if(price<=0)
            else{
               axios.post('{{ url("AddProducts") }}', {
                  sku:sku,
                  product_category_id:product_category_id,
                  product_category_parent_id:product_category_parent_id,
                  name:name,
                  slug:slug,
                  color_id:color_id,
                  description:description,
                  price:price,
                  visible:visible,
                  file:file,
                  image_name:image_name,
               }).then(function (response) {
                  if(response.data.status=="success"){
                     self.products=response.data.products;
                     self.clear();
                     $.Notification.autoHideNotify('success', 'top right', 'Información','Registro Satisfactorio.');
                  }//if(response.data.status=="success")
                  else if(response.data.status=="error"){                     
                     $.each(response.data.mensaje, function( key, value ) {
                        $.Notification.autoHideNotify('error', 'top right', 'Información',''+value);
                     });
                  }//if(response.data.status=="error")
               }).catch(function (error) {
                  console.log(error);
               });
               /*Fin envio de datos a la base de datos*/
            }//else if(name=="")   
         },//register
         captureRecord: function(value){
            this.data.sku=value.sku;
            this.data.product_category_id=value.product_category_id;
            this.data.product_category_parent_id=value.product_category_parent_id;
            var name=$("#name").val(value.name);
            var slug=$("#slug").val(value.slug);
            this.data.color_id=value.color_id;
            this.data.description=value.description;
            this.data.price=value.price;
            if(value.visible==0)
             this.data.visible=false;
            else
             this.data.visible=true;
            this.data.file=value.file;
            this.data.image_name=value.image_name;
            this.id=value.id;         
            this.product_category_idRequired=false;
            this.skuRequired=false;
            this.nameRequired=false;
            this.slugRequired=false;
            this.color_idRequired=false;
            this.priceRequired=false;
            this.descriptionRequired=false;
            this.buttonNameUno="Cancelar";
            this.buttonNameDos="Actualizar";
            this.change=1;
         },
         cancel:function(){
            this.clear();
            this.id="";      
            this.buttonNameUno="Limpiar";
            this.buttonNameDos="Registrar";   
            this.buttonShowDos=true;
            this.change=0;
         },
         update:function(){
            let self = this;
            var id=this.id;
            var sku=this.data.sku.toUpperCase();
            var product_category_id=this.data.product_category_id;
            var product_category_parent_id=this.data.product_category_parent_id;            
            var name=$("#name").val();
            var slug=$("#slug").val();
            var color_id=this.data.color_id;
            var description=this.data.description;
            var price=this.data.price;
            var visible=this.data.visible;
            if(visible==false)
               visible=0;
            else
               visible=1;
            var file=this.data.file;
            var image_name=this.data.image_name;
            if(image_name=='Selecione una imagen'){
               image_name="";
            }//if(image_name=='Selecione una imagen')
            this.product_category_idRequired=false;
            this.skuRequired=false;
            this.nameRequired=false;
            this.slugRequired=false;
            this.color_idRequired=false;
            this.priceRequired=false;
            this.descriptionRequired=false;
            if(sku==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo código del producto es un campo obligatorio!');
               this.skuRequired=true;
            }//if(sku=="")
            else if(product_category_id==0){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo categoría es un campo obligatorio!');
               this.product_category_idRequired=true;
            }//else if(product_category_id==0)
            else if(name==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo nombre es un campo obligatorio!');
               this.nameRequired=true;
               this.slugRequired=true;
            }//else if(name=="")
            else if(color_id==0){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo color es un campo obligatorio!');
               this.color_idRequired=true;
            }//else if(color_id==0)
            else if(description==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo descripción es un campo obligatorio!');
               this.descriptionRequired=true;
            }//else if(description=="")
            else if(price==""){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo precio es un campo obligatorio!');
               this.priceRequired=true;
            }//else if(price=="")
            else if(price<=0){
               $.Notification.autoHideNotify('error', 'top right', 'Información','El campo precio debe mayor a cero!');
               this.priceRequired=true;
            }//else if(price<=0)
            else{
               axios.post('{{ url("UpdateProducts") }}', {
                  id:id,
                  sku:sku,
                  product_category_id:product_category_id,
                  product_category_parent_id:product_category_parent_id,
                  name:name,
                  slug:slug,
                  color_id:color_id,
                  description:description,
                  price:price,
                  visible:visible,
                  file:file,
                  image_name:image_name,
               }).then(function (response) {
                  if(response.data.status=="success"){
                     self.products=response.data.products;
                     self.cancel();
                     $.Notification.autoHideNotify('success', 'top right', 'Información','Actualización Satisfactoria.');
                  }//if(response.data.status=="success")
                  else if(response.data.status=="error"){                     
                     $.each(response.data.mensaje, function( key, value ) {
                        $.Notification.autoHideNotify('error', 'top right', 'Información',''+value);
                     });
                  }//if(response.data.status=="error")
               }).catch(function (error) {
                  console.log(error);
               });
               /*Fin envio de datos a la base de datos*/
            }//else if(name=="")   
         },//update:function()
         destroy:function(value){
            let self = this;
            var id=value.id;  
            Swal({
                 title: 'Información',
                 text: "¿Esta seguro de eliminar este registro?",
                 type: 'warning',
                 showCancelButton: true,
                 confirmButtonColor: '#3085d6',
                 cancelButtonColor: '#d33',
                 confirmButtonText: '¡Sí, bórralo!!'
            }).then((result) => {
             if (result.value) {
               //
               axios.post('{{ url("DeleteProducts") }}', {id:id}).then(response => {
                  if(response.data.status=="success"){
                     self.products=response.data.products;
                     self.clear();
                     $.Notification.autoHideNotify('success', 'top right', 'Información','Registro eliminado con éxito.');
                  }//if(response.data.status=="success")
                  if(response.data.status=="error"){
                     self.clear();                           
                     $.each(response.data.mensaje, function( key, value ) {
                        $.Notification.autoHideNotify('error', 'top right', 'Información',''+value);
                     });
                  }//if(response.data.status=="error")
               }).catch(error => {
                         console.log(error);
               });
               //
            }
          })
         },//destroy:function()
         ActionsCru:function(){
           if(this.change==0){
             this.register();
           }//if(this.change==0)
           else if(this.change==1){
             this.update();
           }//else if(this.change==1)
         },//ActionsCru:function()
         ActionsCc:function(){
            if(this.change==0){
             this.clear();
           }//if(this.change==0)
           else if(this.change==1){
             this.cancel();
           }//else if(this.change==1)
         }//ActionsCc:function()
        },//methods
   });//const app= new Vue
</script>
<script>
   $(document).ready(function(){
     $("#name,#slug").stringToSlug({
        callback:function(text){
           $("#slug").val(text);
        }
     });
   });
</script>
@endpush


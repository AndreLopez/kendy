<!DOCTYPE html>
<html lang="es-Es">
<header class="header main__header">
	<meta charset="UTF-8">
	<title>Kendys · Compras</title>
	
	{{-- Hojas de estilos --}}
	@include('layouts.headstore')
	@include('layouts.modulos.navbar')
	
</header>
<body>
{{-- Secciones --}}


<section class="blogshow sections__blog">
	  
		<h1>Detalles del Producto</h1>
	
		<div class="blog--content-card grid column-2 gap-20">
			
							
			<div class="blog--imgshow">
					  <img src="{{asset($product->image_name)}}">
			</div>
		
			<div class="products-block">
				<h3>{{$product->name}}</h3> <hr>
			
			
				<div class="blog--text">
						<p> {{$product->description}}</p>
						<p> Precio: ${{number_format($product->price,2)}}</p>
						<p>
							<a href="#">Shopping</a>
							<a href="{{route('store') }}">Back</a>
						</p>
												
				</div>	
				
							
			</div>
			
						
					
								
				
</div>	
</section>	
			
			


		{{-- Barra de navegación  --}}
		@include('layouts.modulos.contact-us-bar')
		{{-- Pie de página --}}
		@include('layouts.modulos.footer')
</body>
</html>



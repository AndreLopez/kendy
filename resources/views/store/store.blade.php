<!DOCTYPE html>
<html lang="es-Es">
<header class="header main__header">
	<meta charset="UTF-8">
	<title>Kendys · Compras</title>
	
	{{-- Hojas de estilos --}}
	@include('layouts.headstore')
	@include('layouts.modulos.navbar')
	
</header>
<body>

<div class="blog--content-card grid column-3 gap-5">

<section class="blogcat sections__blogcat">
		<div class="blog--text" >
			<h1> Colecciones</h1>
			<p href="#">Todas</p>
			@foreach($products as $search)
			<a href="{{route ('categories-search', $search->product_category_id)}}"><p>Primavera</p></a>
			
		@endforeach
		</div>	
</section>		


	

<section class="blog sections__blog">
		<div class= "blog--content-card grid column-4 gap-10">
			
			@foreach ($products as $product)
												
				<div class="blog--card">
			
					<div class="blog--img">
					  <img src="{{asset($product->image_name)}}">
					</div>
		
					<div class="blog--text">
						<p> {{$product->sku}}</p>
						<p> Precio: ${{number_format($product->price,2)}}</p>
						<p>
							<a href="#">Shopping</a>
							<a href="{{route('store-detail',$product->slug)}}">View</a>
						</p>
							
					</div>	
					
				</div>	
				
						
			@endforeach
		</div>
		
</section>		


<section class="blogcat1 sections__blogcat1">
	
	<div class="blog--text">
			
			<h3>Orders</h3>
			{{-- Campo de la tabla que indica cuantos productos van en el carrito  --}}
			
			<div class="blog--imgshopping">
				<img src="{{ asset('images/iconos/shopping-cart.svg') }}" >
			</div>	
								
	</div>	
	
		
</section>		

</div>	

		{{-- Barra de navegación  --}}
		@include('layouts.modulos.contact-us-bar')
		{{-- Pie de página --}}
		@include('layouts.modulos.footer')
</body>
</html>

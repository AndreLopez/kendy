<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

// Route::get('/', 'Web\HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {
	Route::group(['middleware' => ['auth','admin']], function () {
		Route::get('/dashboard', 'Dashboard\DashboardController@index')->name('dashboard');
		/************************************Ajustes**************************************/
		// Usuarios
		Route::get('/users', 'Dashboard\Settings\UsersController@index')->name('users');
		Route::post('/AddUsers','Dashboard\Settings\UsersController@store')->name('AddUsers');
		Route::post('/UpdateUsers','Dashboard\Settings\UsersController@update')->name('UpdateUsers');
		Route::post('/DeleteUsers','Dashboard\Settings\UsersController@destroy')->name('DeleteUsers');
		Route::get('/profile','Dashboard\Settings\UsersController@profile')->name('profile');
		Route::get('/loadUser','Dashboard\Settings\UsersController@loadUser')->name('loadUser');
		/************************************Tienda**************************************/
		// Monedas
		Route::get('/coins', 'Dashboard\Store\CoinsController@index')->name('coins');
		Route::post('/AddCoins','Dashboard\Store\CoinsController@store')->name('AddCoins');
		Route::post('/UpdateCoins','Dashboard\Store\CoinsController@update')->name('UpdateCoins');
		Route::post('/DeleteCoins','Dashboard\Store\CoinsController@destroy')->name('DeleteCoins');
	
		// Colores de Productos
		Route::get('/productsColors', 'Dashboard\Store\ProductsColorsController@index')->name('productsColors');
		Route::post('/AddProductsColors','Dashboard\Store\ProductsColorsController@store')->name('AddProductsColors');
		Route::post('/UpdateProductsColors','Dashboard\Store\ProductsColorsController@update')->name('UpdateProductsColors');
		Route::post('/DeleteProductsColors','Dashboard\Store\ProductsColorsController@destroy')->name('DeleteProductsColors');
	
		// Categorías de Productos
		Route::get('/productsCategories', 'Dashboard\Store\ProductsCategoriesController@index')->name('productsCategories');
		Route::post('/AddProductsCategories','Dashboard\Store\ProductsCategoriesController@store')->name('AddpProductsCategories');
		Route::post('/UpdateProductsCategories','Dashboard\Store\ProductsCategoriesController@update')->name('UpdateProductsCategories');
		Route::post('/DeleteProductsCategories','Dashboard\Store\ProductsCategoriesController@destroy')->name('DeleteProductsCategories');
	
		// Productos
		Route::get('/products', 'Dashboard\Store\ProductsController@index')->name('products');
		Route::post('/AddProducts','Dashboard\Store\ProductsController@store')->name('AddpProducts');
		Route::post('/UpdateProducts','Dashboard\Store\ProductsController@update')->name('UpdateProducts');
		Route::post('/DeleteProducts','Dashboard\Store\ProductsController@destroy')->name('DeleteProducts');
		Route::post('/FindSubCategories','Dashboard\Store\ProductsController@findSubCategories')->name('FindSubCategories');
	
		// Ordenes
		Route::get('/orders', 'Dashboard\Store\OrdersController@index')->name('orders');
		Route::post('/DeleteOrders','Dashboard\Store\Orders@destroy')->name('DeleteOrders');
	
		/************************************Blog**************************************/
		// Etiquetas
		Route::get('/tags', 'Dashboard\Blog\TagsController@index')->name('tags');
		Route::post('/AddTags','Dashboard\Blog\TagsController@store')->name('AddTags');
		Route::post('/UpdateTags','Dashboard\Blog\TagsController@update')->name('UpdateTags');
		Route::post('/DeleteTags','Dashboard\Blog\TagsController@destroy')->name('DeleteTags');
	
		// Categorías
		Route::get('/categories', 'Dashboard\Blog\CategoriesController@index')->name('categories');
		Route::post('/AddCategories','Dashboard\Blog\CategoriesController@store')->name('AddCategories');
		Route::post('/UpdateCategories','Dashboard\Blog\CategoriesController@update')->name('UpdateCategories');
		Route::post('/DeleteCategories','Dashboard\Blog\CategoriesController@destroy')->name('DeleteCategories');
	
		// Entradas
		Route::get('/posts', 'Dashboard\Blog\PostsController@index')->name('posts');
		Route::post('/AddPosts','Dashboard\Blog\PostsController@store')->name('AddPosts');
		Route::post('/UpdatePosts','Dashboard\Blog\PostsController@update')->name('UpdatePosts');
		Route::post('/DeletePosts','Dashboard\Blog\PostsController@destroy')->name('DeletePosts');
	});	
});
//Backend carrito de compras
Route::get('/getShoppingCart', 'Dashboard\Store\ShoppingCartController@showShoppingCart')->name('getShoppingCart');
Route::post('/addShoppingCart', 'Dashboard\Store\ShoppingCartController@addShoppingCart')->name('addShoppingCart');
Route::post('/deleteProductShowShoppingCart', 'Dashboard\Store\ShoppingCartController@deleteProductShowShoppingCart')->name('deleteProductShowShoppingCart');
Route::post('/updateProductShowShoppingCart', 'Dashboard\Store\ShoppingCartController@updateProductShowShoppingCart')->name('updateProductShowShoppingCart');
Route::get('/destroyShowShoppingCart', 'Dashboard\Store\ShoppingCartController@destroyShowShoppingCart')->name('destroyShowShoppingCart');

//Carrito de compras
Route::get('/store', 'StoreController@index')->name('store');
Route::get('/store-detail/{slug}', 'StoreController@show')->name('store-detail');

//Filtrado de categorias
Route::get('categories-search/{$product_category_id}', 'StoreController@searchCategory')->name('categories-search');

// Principal
Route::get("/", function() {
	return view("master");
});